class BankInforModel {
  List<DataBank> data = [];

  BankInforModel({this.data});

  BankInforModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <DataBank>[];
      json['data'].forEach((v) {
        data.add(new DataBank.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataBank {
  String bankName;
  String holderName;
  String accountNo;
  String bankLogo;
  String bankUrl;

  DataBank({this.bankName, this.holderName, this.accountNo, this.bankUrl});

  DataBank.fromJson(Map<String, dynamic> json) {
    bankName = json['bank_name'];
    holderName = json['holder_name'];
    accountNo = json['account_no'];
    bankLogo = json['bank_logo'];
    bankUrl = json['bank_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bank_name'] = this.bankName;
    data['holder_name'] = this.holderName;
    data['account_no'] = this.accountNo;
    data['bank_logo'] = this.bankLogo;
    data['bank_url'] = this.bankUrl;
    return data;
  }
}
