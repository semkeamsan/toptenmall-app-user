class VideoBannerModel {
  int _id;
  String _videoId;
  int _published;
  String _createdAt;
  String _updatedAt;
  String _url;

  VideoBannerModel(
      {int id,
      String videoId,
      int published,
      String createdAt,
      String updatedAt,
      String url}) {
    this._id = id;
    this._videoId = videoId;
    this._published = published;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._url = url;
  }

  int get id => _id;
  String get videoId => _videoId;
  int get published => _published;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get url => _url;

  VideoBannerModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _videoId = json['video_id'];
    _published = json['published'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['video_id'] = this._videoId;
    data['published'] = this._published;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['url'] = this._url;
    return data;
  }
}
