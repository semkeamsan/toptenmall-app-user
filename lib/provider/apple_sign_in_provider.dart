import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AppleSignInProvider with ChangeNotifier {
  final _firebaseAuth = FirebaseAuth.instance;
  AuthorizationCredentialAppleID auth;

  Future<User> login() async {
    auth = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      webAuthenticationOptions: WebAuthenticationOptions(
        clientId: 'com.eshopkh.app',
        redirectUri: Uri.parse(
          AppConstants.BASE_URL + '/customer/auth/login/apple/callback',
        ),
      ),
    );
    final oAuthProvider = OAuthProvider('apple.com');
    final credential = oAuthProvider.credential(
      idToken: auth.identityToken,
      accessToken: auth.authorizationCode,
    );
    final userCredential = await _firebaseAuth.signInWithCredential(credential);
    final firebaseUser = userCredential.user;

    if (auth.givenName != null && auth.familyName != null) {
      final displayName = '${auth.givenName} ${auth.familyName}';
      await firebaseUser.updateDisplayName(displayName);
    }
    notifyListeners();
    return firebaseUser;
  }
}
