import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DateConverter {
  static String formatDate(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'yyyy-MM-dd hh:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime);
  }

  static String estimatedDate(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'dd MMM yyyy',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime);
  }

  static DateTime convertStringToDatetime(
      BuildContext context, String dateTime) {
    return DateFormat(
            "yyyy-MM-dd hh:mm:ss",
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .parse(dateTime, true);
  }

  static DateTime isoStringToLocalDate(BuildContext context, String dateTime) {
    return DateFormat(
            'yyyy-MM-dd HH:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .parse(dateTime, true)
        .toLocal();
  }

  static String localDateToIsoStringAMPM(
      BuildContext context, DateTime dateTime) {
    return DateFormat(
            'h:mm a | d-MMM-yyyy ',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime.toLocal());
  }

  static String isoStringToLocalTimeOnly(
      BuildContext context, String dateTime) {
    return DateFormat('HH:mm').format(isoStringToLocalDate(context, dateTime));
  }

  static String isoStringToLocalDateOnly(
      BuildContext context, String dateTime) {
    return DateFormat(
            'dd-MMM-yyyy',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(isoStringToLocalDate(context, dateTime));
  }

  static String localDateToIsoString(BuildContext context, DateTime dateTime) {
    return DateFormat(
            'yyyy-MM-dd HH:mm:ss',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(dateTime.toUtc());
  }

  static String isoStringToLocalDateAndTime(
      BuildContext context, String dateTime) {
    return DateFormat(
            'dd-MMM-yyyy hh:mm a',
            Provider.of<LocalizationProvider>(context, listen: false)
                .locale
                .languageCode)
        .format(isoStringToLocalDate(context, dateTime));
  }
}
