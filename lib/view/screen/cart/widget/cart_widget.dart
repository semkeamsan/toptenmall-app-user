import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/cart_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class CartWidget extends StatelessWidget {
  final CartModel cartModel;
  final int index;
  final bool fromCheckout;
  const CartWidget(
      {Key key,
      this.cartModel,
      @required this.index,
      @required this.fromCheckout});

  @override
  Widget build(BuildContext context) {
    // var map = Map.fromIterable(cartModel.variations.entries,
    //     key: (e) => e.keys.first, value: (e) => e.values.first);

    return Container(
      margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      decoration: BoxDecoration(color: Theme.of(context).highlightColor),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*

        !fromCheckout ? InkWell(
          onTap: () => Provider.of<CartProvider>(context, listen: false).toggleSelected(index),
          child: Container(
            width: 15,
            height: 15,
            margin: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: ColorResources.getPrimary(context), width: 1),
            ),
            // child: Provider.of<CartProvider>(context).isSelectedList[index]
            //     ? Icon(Icons.done, color: ColorResources.getPrimary(context), size: Dimensions.ICON_SIZE_EXTRA_SMALL)
            //     : SizedBox.shrink(),
          ),
        ) : SizedBox.shrink(),*/

            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.withOpacity(.2),
                ),
              ),
              child: CachedNetworkImage(
                height: Responsive.isMobile(context) ? 100 : 200,
                width: Responsive.isMobile(context) ? 100 : 200,
                fit: BoxFit.cover,
                imageUrl: cartModel.thumbnail != ''
                    ? '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${cartModel.thumbnail}'
                    : Images.placeholder,
                placeholder: (c, o) => Image.asset(
                  Images.placeholder,
                  height: Responsive.isMobile(context) ? 100 : 200,
                  width: Responsive.isMobile(context) ? 100 : 200,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Dimensions.PADDING_SIZE_LARGE),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      cartModel.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_LARGE
                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                        color: Colors.black,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          PriceConverter.convertPrice(
                              context, cartModel.price - cartModel.discount),
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          getTranslated('shipping_cost', context) + ' :',
                          style: TextStyle(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                          ),
                        ),
                        Text(
                          PriceConverter.convertPrice(
                              context, cartModel.shippingCost),
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.PADDING_SIZE_DEFAULT,
                    ),
                    cartModel.discount > 0
                        ? Column(
                            children: [
                              Container(
                                width: 80,
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      Dimensions.PADDING_SIZE_EXTRA_SMALL,
                                ),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color:
                                          ColorResources.getPrimary(context)),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(3.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        PriceConverter.percentageCalculation(
                                            context,
                                            cartModel.price,
                                            cartModel.discount,
                                            'amount'),
                                        textAlign: TextAlign.center,
                                        style: titilliumRegular.copyWith(
                                            fontSize: Dimensions
                                                .FONT_SIZE_EXTRA_SMALL,
                                            color: ColorResources.getHint(
                                                context)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: Dimensions.PADDING_SIZE_DEFAULT,
                              ),
                            ],
                          )
                        : SizedBox(),
                    Provider.of<AuthProvider>(context, listen: false)
                            .isLoggedIn()
                        ? Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    right: Dimensions.PADDING_SIZE_SMALL),
                                child: QuantityButton(
                                    isIncrement: false,
                                    index: index,
                                    quantity: cartModel.quantity,
                                    maxQty: 20,
                                    cartModel: cartModel),
                              ),
                              Text(cartModel.quantity.toString(),
                                  style: titilliumSemiBold.copyWith(
                                    fontSize: Responsive.isMobile(context)
                                        ? Dimensions.FONT_SIZE_DEFAULT
                                        : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                  )),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: Dimensions.PADDING_SIZE_SMALL),
                                child: QuantityButton(
                                    index: index,
                                    isIncrement: true,
                                    quantity: cartModel.quantity,
                                    maxQty: 20,
                                    cartModel: cartModel),
                              ),
                            ],
                          )
                        : SizedBox.shrink(),
                    (cartModel.variant != null && cartModel.variant.isNotEmpty)
                        ? Padding(
                            padding: EdgeInsets.only(
                                top: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      '${getTranslated('variations', context)}: ',
                                      style: titilliumSemiBold.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                      )),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: List.generate(
                                        cartModel.variations.values.length,
                                        (index) {
                                      return Container(
                                        child: Row(
                                          children: [
                                            Text(
                                                cartModel.variations.keys
                                                        .elementAt(index)
                                                        .toString()
                                                        .toUpperCase() +
                                                    ' : ',
                                                style:
                                                    titilliumRegular.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                )),
                                            Text(
                                                cartModel.variations.values
                                                    .elementAt(index)
                                                    .toString(),
                                                style: titilliumBold.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD,
                                                )),
                                          ],
                                        ),
                                      );
                                    }),
                                  ),
                                ]),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ),
            !fromCheckout
                ? IconButton(
                    onPressed: () {
                      if (Provider.of<AuthProvider>(context, listen: false)
                          .isLoggedIn()) {
                        Provider.of<CartProvider>(context, listen: false)
                            .removeFromCartAPI(context, cartModel.id);
                      } else {
                        Provider.of<CartProvider>(context, listen: false)
                            .removeFromCart(index);
                      }
                    },
                    icon: Icon(Icons.cancel, color: ColorResources.RED),
                  )
                : SizedBox.shrink(),
          ]),
    );
  }
}

class QuantityButton extends StatelessWidget {
  final CartModel cartModel;
  final bool isIncrement;
  final int quantity;
  final int index;
  final int maxQty;
  QuantityButton(
      {@required this.isIncrement,
      @required this.quantity,
      @required this.index,
      @required this.maxQty,
      @required this.cartModel});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (!isIncrement && quantity > 1) {
          // Provider.of<CartProvider>(context, listen: false).setQuantity(false, index);
          Provider.of<CartProvider>(context, listen: false)
              .updateCartProductQuantity(
                  cartModel.id, cartModel.quantity - 1, context)
              .then((value) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(value.message),
              backgroundColor: value.isSuccess ? Colors.green : Colors.red,
            ));
          });
        } else if (isIncrement && quantity < maxQty) {
          // Provider.of<CartProvider>(context, listen: false).setQuantity(true, index);
          Provider.of<CartProvider>(context, listen: false)
              .updateCartProductQuantity(
                  cartModel.id, cartModel.quantity + 1, context)
              .then((value) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(value.message),
              backgroundColor: value.isSuccess ? Colors.green : Colors.red,
            ));
          });
        }
      },
      child: Icon(
        isIncrement ? Icons.add_circle : Icons.remove_circle,
        color: isIncrement
            ? quantity >= maxQty
                ? ColorResources.getGrey(context)
                : ColorResources.getPrimary(context)
            : quantity > 1
                ? ColorResources.getPrimary(context)
                : ColorResources.getGrey(context),
        size: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
      ),
    );
  }
}
