import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/promotion/widgets/promotion_product_view.dart';
import 'package:provider/provider.dart';

class PromotionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isGuestMode =
        !Provider.of<AuthProvider>(context, listen: false).isLoggedIn();
    if (!isGuestMode) {
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressTypeList(context);
      Provider.of<ProfileProvider>(context, listen: false)
          .initAddressList(context);
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: BackButton(
            color: Theme.of(context).textTheme.bodyText1.color,
            onPressed: () {
              return Navigator.pop(context);
            }),
        title: Text(
          "Promotions",
          style: TextStyle(
            fontSize: Dimensions.FONT_SIZE_LARGE,
            color: Colors.black,
          ),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_SMALL,
                  vertical: Dimensions.PADDING_SIZE_SMALL),
              child: Column(children: [
                Consumer<FlashDealProvider>(
                    builder: (context, flashDealProvider, child) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl: flashDealProvider.flashDeal.banner != null
                          ? '${Provider.of<SplashProvider>(context, listen: false).baseUrls.flashDealImageUrl}'
                              '/${flashDealProvider.flashDeal.banner}'
                          : Images.placeholder,
                      placeholder: (c, o) =>
                          Image.asset(Images.placeholder, fit: BoxFit.cover),
                    ),
                  );
                }),

                // Flash Deals
                Consumer<FlashDealProvider>(
                  builder: (context, flashDealProvider, child) {
                    return flashDealProvider.flashDealList.length != 0
                        ? Container(
                            child: Column(children: [
                              //flash_deal
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                                child: PromotionProductView(
                                  isHomePage: false,
                                ),
                              ),
                            ]),
                          )
                        : SizedBox.shrink();
                  },
                ),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
