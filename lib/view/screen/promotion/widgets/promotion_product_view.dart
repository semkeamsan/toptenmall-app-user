import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/product_details_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class PromotionProductView extends StatelessWidget {
  final bool isHomePage;
  PromotionProductView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<FlashDealProvider>(
      builder: (context, flashDealProvider, child) {
        return StaggeredGridView.countBuilder(
          itemCount: isHomePage
              ? flashDealProvider.flashDealList.length > 6
                  ? 6
                  : flashDealProvider.flashDealList.length
              : flashDealProvider.flashDealList.length,
          crossAxisCount: 3,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          addAutomaticKeepAlives: true,
          padding: EdgeInsets.zero,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Product productModel = flashDealProvider.flashDealList[index];
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    transitionDuration: Duration(milliseconds: 1000),
                    pageBuilder: (context, anim1, anim2) =>
                        ProductDetails(product: productModel),
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                  borderRadius: BorderRadius.circular(
                    Dimensions.PADDING_SIZE_DEFAULT,
                  ),
                  color: Theme.of(context).highlightColor,
                ),
                child: Stack(
                  children: [
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(
                                Dimensions.PADDING_SIZE_DEFAULT,
                              ),
                              topRight: Radius.circular(
                                Dimensions.PADDING_SIZE_DEFAULT,
                              ),
                            ),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              width: Responsive.isMobile(context) ? 120 : 300,
                              height: Responsive.isMobile(context) ? 120 : 300,
                              imageUrl: Provider.of<SplashProvider>(context,
                                          listen: false)
                                      .baseUrls
                                      .productThumbnailUrl +
                                  '/' +
                                  flashDealProvider
                                      .flashDealList[index].thumbnail,
                              placeholder: (c, s) => Image.asset(
                                Images.placeholder,
                                fit: BoxFit.cover,
                                width: Responsive.isMobile(context) ? 120 : 300,
                                height:
                                    Responsive.isMobile(context) ? 120 : 300,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.PADDING_SIZE_SMALL,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: Dimensions.PADDING_SIZE_SMALL),
                            child: Text(
                              PriceConverter.convertPrice(
                                  context,
                                  flashDealProvider
                                          .flashDealList[index].unitPrice ??
                                      0.0,
                                  discountType: flashDealProvider
                                      .flashDealList[index].discountType,
                                  discount: flashDealProvider
                                      .flashDealList[index].discount),
                              style: robotoBold.copyWith(
                                color: ColorResources.getRed(context),
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                              Dimensions.PADDING_SIZE_SMALL,
                              0.0,
                              Dimensions.PADDING_SIZE_SMALL,
                              0.0,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  bottom: Dimensions.PADDING_SIZE_DEFAULT),
                              child: Text(
                                PriceConverter.convertPrice(
                                    context,
                                    flashDealProvider
                                        .flashDealList[index].unitPrice),
                                maxLines: 1,
                                style: robotoBold.copyWith(
                                  color: Theme.of(context).hintColor,
                                  decoration: TextDecoration.lineThrough,
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_SMALL
                                      : Dimensions.FONT_SIZE_SMALL_IPAD,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                        height: Responsive.isMobile(context) ? 50.0 : 100.0,
                        width: Responsive.isMobile(context) ? 50.0 : 100.0,
                        padding: EdgeInsets.symmetric(
                            horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                        decoration: BoxDecoration(
                          color: ColorResources.getRed(context),
                          borderRadius:
                              BorderRadius.only(topRight: Radius.circular(10)),
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                              ),
                              Text(
                                getTranslated("SAVE", context),
                                style: robotoRegular.copyWith(
                                  color: Colors.white,
                                  fontSize: Responsive.isMobile(context)
                                      ? Dimensions.FONT_SIZE_SMALL
                                      : Dimensions.FONT_SIZE_SMALL_IPAD,
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    flashDealProvider.flashDeal.title
                                        .replaceAll(new RegExp('[^0-9]'), ''),
                                    style: robotoRegular.copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_SMALL
                                          : Dimensions.FONT_SIZE_SMALL_IPAD,
                                    ),
                                  ),
                                  Text(
                                    "%",
                                    style: robotoRegular.copyWith(
                                      color: Colors.white,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_DEFAULT
                                          : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}

class BrandShimmer extends StatelessWidget {
  final bool isHomePage;
  BrandShimmer({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: (1 / 1.3),
        mainAxisSpacing: 10,
        crossAxisSpacing: 3,
      ),
      itemCount: isHomePage ? 6 : 30,
      shrinkWrap: true,
      physics: isHomePage ? NeverScrollableScrollPhysics() : null,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled:
              Provider.of<ProductProvider>(context).latestProductList.length ==
                  0,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(color: ColorResources.WHITE),
              ),
            ),
          ]),
        );
      },
    );
  }
}
