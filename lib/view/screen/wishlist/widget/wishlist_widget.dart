import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/wishlist_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/cart_bottom_sheet.dart';
import 'package:provider/provider.dart';

class WishListWidget extends StatelessWidget {
  final Product product;
  final int index;
  WishListWidget({this.product, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      margin: EdgeInsets.only(top: Dimensions.MARGIN_SIZE_SMALL),
      decoration: BoxDecoration(
          color: Theme.of(context).highlightColor,
          borderRadius: BorderRadius.circular(5)),
      child: GestureDetector(
        onTap: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.withOpacity(.4)),
              ),
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                width: Responsive.isMobile(context) ? 45 : 100,
                height: Responsive.isMobile(context) ? 45 : 100,
                imageUrl:
                    '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${product.thumbnail}',
                placeholder: (c, o) => Image.asset(
                  Images.placeholder,
                  fit: BoxFit.cover,
                  width: Responsive.isMobile(context) ? 45 : 100,
                  height: Responsive.isMobile(context) ? 45 : 100,
                ),
              ),
            ),
            SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
            Expanded(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          product.name ?? '',
                          style: titilliumRegular.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            color: ColorResources.getPrimary(context),
                          ),
                        ),
                      ),
                      CircleAvatar(
                        backgroundColor: Theme.of(context).hintColor,
                        radius: Responsive.isMobile(context) ? 10 : 20,
                        child: IconButton(
                          padding: EdgeInsets.all(0),
                          alignment: Alignment.center,
                          icon: Icon(
                            Icons.edit,
                            color: Theme.of(context).highlightColor,
                            size: Responsive.isMobile(context)
                                ? Dimensions.ICON_SIZE_SMALL
                                : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                          ),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (_) => new CupertinoAlertDialog(
                                      title: new Text(
                                        getTranslated(
                                            'ARE_YOU_SURE_WANT_TO_REMOVE_WISH_LIST',
                                            context),
                                        style: titilliumRegular.copyWith(
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_DEFAULT
                                              : Dimensions
                                                  .FONT_SIZE_DEFAULT_IPAD,
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          child: Text(
                                            getTranslated('YES', context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            ),
                                          ),
                                          onPressed: () {
                                            print(product.id);

                                            Provider.of<WishListProvider>(
                                                    context,
                                                    listen: false)
                                                .removeWishList(product.id,
                                                    index: index);
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: Text(
                                            getTranslated('NO', context),
                                            style: titilliumRegular.copyWith(
                                              fontSize: Responsive.isMobile(
                                                      context)
                                                  ? Dimensions.FONT_SIZE_DEFAULT
                                                  : Dimensions
                                                      .FONT_SIZE_DEFAULT_IPAD,
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    ));
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  Row(
                    children: [
                      Text(
                        product.unitPrice != null
                            ? PriceConverter.convertPrice(
                                context, product.unitPrice)
                            : '',
                        style: titilliumSemiBold.copyWith(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_DEFAULT
                                : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                            color: ColorResources.getPrimary(context)),
                      ),
                      Expanded(
                        child: Text(
                          'x${product.minQty}',
                          style: titilliumSemiBold.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: ColorResources.getPrimary(context)),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: Dimensions.PADDING_SIZE_SMALL),
                        padding: EdgeInsets.symmetric(
                            horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(
                                color: ColorResources.getPrimary(context))),
                        child: Text(
                          product.unitPrice != null &&
                                  product.discount != null &&
                                  product.discountType != null
                              ? PriceConverter.percentageCalculation(
                                  context,
                                  product.unitPrice,
                                  product.discount,
                                  product.discountType)
                              : '',
                          style: titilliumRegular.copyWith(
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_DEFAULT
                                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                              color: ColorResources.getPrimary(context)),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              isScrollControlled: true,
                              backgroundColor: Colors.transparent,
                              builder: (con) =>
                                  CartBottomSheet(product: product));
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: Dimensions.PADDING_SIZE_SMALL),
                          padding: EdgeInsets.symmetric(
                              horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 1,
                                  blurRadius: 7,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                              gradient: LinearGradient(colors: [
                                Theme.of(context).primaryColor,
                                Theme.of(context).primaryColor,
                                Theme.of(context).primaryColor,
                              ]),
                              borderRadius: BorderRadius.circular(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.shopping_cart,
                                color: Colors.white,
                                size: Responsive.isMobile(context)
                                    ? Dimensions.ICON_SIZE_DEFAULT
                                    : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                              ),
                              FittedBox(
                                child: Text(
                                    getTranslated('add_to_cart', context),
                                    style: titilliumBold.copyWith(
                                        fontSize: Responsive.isMobile(context)
                                            ? Dimensions.FONT_SIZE_DEFAULT
                                            : Dimensions.FONT_SIZE_DEFAULT_IPAD,
                                        color: Colors.white)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
