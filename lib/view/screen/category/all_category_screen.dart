import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/top_seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/widgets/category_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/top_seller_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/topSeller/all_top_seller_screen.dart';
import 'package:provider/provider.dart';

import '../search/search_screen.dart';

class AllCategoryScreen extends StatelessWidget {
  final bool isBackButtonExist;
  AllCategoryScreen({this.isBackButtonExist = true});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      appBar: AppBar(
        toolbarHeight: 80.0,
        backgroundColor: Theme.of(context).primaryColor,
        //   title: Container(
        //     alignment: Alignment.center,
        //     child: GestureDetector(
        //       onTap: () => Navigator.push(
        //         context,
        //         MaterialPageRoute(
        //           builder: (_) => SearchScreen(),
        //         ),
        //       ),
        //       child: Row(
        //         mainAxisAlignment: MainAxisAlignment.center,
        //         children: [
        //           Icon(Icons.search),
        //           SizedBox(
        //             width: 10.0,
        //           ),
        //           Text(
        //             "Search Product...",
        //             style: TextStyle(fontSize: Dimensions.FONT_SIZE_DEFAULT),
        //           ),
        //         ],
        //       ),
        //     ),
        //   ),
        //   actions: [
        //     Padding(
        //       padding: EdgeInsets.symmetric(
        //           horizontal: Dimensions.PADDING_SIZE_DEFAULT),
        //       child: Row(children: [
        //         IconButton(
        //           onPressed: () {
        //             Navigator.push(
        //                 context, MaterialPageRoute(builder: (_) => CartScreen()));
        //           },
        //           icon: Stack(clipBehavior: Clip.none, children: [
        //             Image.asset(
        //               Images.cart_image,
        //               height: Dimensions.ICON_SIZE_DEFAULT,
        //               width: Dimensions.ICON_SIZE_DEFAULT,
        //               color: Colors.white,
        //             ),
        //             Positioned(
        //               top: -4,
        //               right: -4,
        //               child:
        //                   Consumer<CartProvider>(builder: (context, cart, child) {
        //                 return CircleAvatar(
        //                   radius: 7,
        //                   backgroundColor: ColorResources.RED,
        //                   child: Text(cart.cartList.length.toString(),
        //                       style: titilliumSemiBold.copyWith(
        //                         color: ColorResources.WHITE,
        //                         fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
        //                       )),
        //                 );
        //               }),
        //             ),
        //           ]),
        //         ),
        //         // SizedBox(
        //         //   width: 5.0,
        //         // ),
        //         // Icon(Icons.more_horiz),
        //       ]),
        //     )
        //   ],
        //
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (_) => SearchScreen())),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 2),
                color: Theme.of(context).primaryColor,
                alignment: Alignment.center,
                child: Container(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                height: Responsive.isMobile(context)?45:55,
                  width: MediaQuery.of(context).size.width -
                      (isBackButtonExist ? 140.0 : 80.0),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                        Dimensions.PADDING_SIZE_EXTRA_LARGE),
                    color: Theme.of(context).primaryColor,
                    boxShadow: [
                      BoxShadow(
                          color: ColorResources.getYellow(context),
                          spreadRadius: 1,
                          blurRadius: .5)
                    ],
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            getTranslated("SEARCH_NOW", context),
                            overflow: TextOverflow.ellipsis,
                            style: robotoRegular.copyWith(
                                color: Theme.of(context).hintColor),
                          ),
                        ),
                        Icon(
                          Icons.search,
                          color: ColorResources.getYellow(context),
                          size: 25.0,
                        ),
                      ]),
                ),
              ),
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => CartScreen()));
            },
            icon: Stack(clipBehavior: Clip.none, children: [
              Image.asset(
                Images.cart_arrow_down_image,
                height: Dimensions.ICON_SIZE_DEFAULT,
                width: Dimensions.ICON_SIZE_DEFAULT,
                color: Colors.white,
              ),
              Positioned(
                top: -4,
                right: -4,
                child: Consumer<CartProvider>(builder: (context, cart, child) {
                  return CircleAvatar(
                    radius: 7,
                    backgroundColor: ColorResources.RED,
                    child: Text(cart.cartList.length.toString(),
                        style: titilliumSemiBold.copyWith(
                          color: ColorResources.WHITE,
                          fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                        )),
                  );
                }),
              ),
            ]),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Consumer<TopSellerProvider>(
                builder: (context, topSellerProvider, child) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: Dimensions.PADDING_SIZE_DEFAULT,
                        left: Dimensions.PADDING_SIZE_DEFAULT,
                        right: Dimensions.PADDING_SIZE_DEFAULT),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          getTranslated("SHOP_FOR_YOU", context),
                          style: TextStyle(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_LARGE
                                : Dimensions.FONT_SIZE_LARGE_IPAD,
                            fontWeight: FontWeight.bold,
                            color: ColorResources.BLACK,
                          ),
                        ),
                        topSellerProvider.topSellerList.length > 4
                            ? InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => AllTopSellerScreen(
                                        topSeller: null,
                                      ),
                                    ),
                                  );
                                },
                                child: Text(
                                  getTranslated("SHOP_MORE", context),
                                  style: TextStyle(
                                    fontSize: Dimensions.FONT_SIZE_LARGE,
                                    color: ColorResources.getYellow(context),
                                  ),
                                ),
                              )
                            : SizedBox.shrink(),
                      ],
                    ),
                  ),

                  // Container(
                  //   child: GridView.count(
                  //     physics: NeverScrollableScrollPhysics(),
                  //     padding: const EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                  //     crossAxisSpacing: Dimensions.PADDING_SIZE_SMALL,
                  //     crossAxisCount: 4,
                  //     childAspectRatio: (1 / 0.7),
                  //     shrinkWrap: true,
                  //     children: <Widget>[
                  //       ShopOnCategory(image: "assets/images/prom01.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom02.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom01.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom02.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom01.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom02.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom01.jpg"),
                  //       ShopOnCategory(image: "assets/images/prom02.jpg"),
                  //     ],
                  //   ),
                  // ),

                  //top seller
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Dimensions.PADDING_SIZE_SMALL),
                    child: TopSellerView(isHomePage: true),
                  ),

                  Padding(
                    padding:
                        const EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          getTranslated("CATEGORY", context),
                          style: TextStyle(
                            fontSize: Responsive.isMobile(context)
                                ? Dimensions.FONT_SIZE_LARGE
                                : Dimensions.FONT_SIZE_LARGE_IPAD,
                            fontWeight: FontWeight.bold,
                            color: ColorResources.BLACK,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_DEFAULT),
              child: Column(
                children: [
                  CategoryView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
