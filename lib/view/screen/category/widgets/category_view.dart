import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/brand_and_category_product_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class CategoryView extends StatelessWidget {
  const CategoryView({key}) : super(key: key);

  get robotoRegular => null;

  @override
  Widget build(BuildContext context) {
    return Consumer<CategoryProvider>(
        builder: (context, categoryProvider, child) {
      return categoryProvider.categoryList.length != 0
          ? StaggeredGridView.countBuilder(
              itemCount: categoryProvider.categoryList.length,
              crossAxisCount: Responsive.isMobile(context) ? 3 : 4,
              mainAxisSpacing: 2,
              crossAxisSpacing: 2,
              addAutomaticKeepAlives: true,
              padding: EdgeInsets.all(0),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => BrandAndCategoryProductScreen(
                                  isBrand: false,
                                  id: categoryProvider.categoryList[index].id
                                      .toString(),
                                  name:
                                      categoryProvider.categoryList[index].name,
                                )));
                  },
                  child: Container(
                    margin:
                        EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_SMALL),
                    child: Column(children: [
                      CachedNetworkImage(
                        width: Responsive.isMobile(context) ? 120 : 220,
                        height: Responsive.isMobile(context) ? 120 : 220,
                        fit: BoxFit.cover,
                        imageUrl:
                            Provider.of<SplashProvider>(context, listen: false)
                                    .baseUrls
                                    .categoryImageUrl +
                                '/' +
                                categoryProvider.categoryList[index].icon,
                        placeholder: (c, s) => Image.asset(
                          Images.placeholder,
                          width: Responsive.isMobile(context) ? 120 : 220,
                          height: Responsive.isMobile(context) ? 120 : 220,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        categoryProvider.categoryList[index].name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_LARGE
                              : Dimensions.FONT_SIZE_LARGE_IPAD,
                        ),
                      ),
                    ]),
                  ),
                );
              },
            )
          : BrandShimmer();
    });
  }
}

class BrandShimmer extends StatelessWidget {
  BrandShimmer();

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 5,
        childAspectRatio: (1 / 1.5),
        mainAxisSpacing: 10,
        crossAxisSpacing: 5,
      ),
      itemCount: 30,
      shrinkWrap: true,
      physics: null,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled:
              Provider.of<CategoryProvider>(context).categoryList.length == 0,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
                child: Container(
                    decoration: BoxDecoration(
              color: ColorResources.WHITE,
            ))),
            Container(
                height: 10,
                color: ColorResources.WHITE,
                margin: EdgeInsets.only(left: 25, right: 25)),
          ]),
        );
      },
    );
  }
}
