import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/onboarding_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/auth/auth_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/dashboard/dashboard_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/setting/widget/currency_dialog.dart';
import 'package:provider/provider.dart';

class OnBoardingScreen extends StatelessWidget {
  final Color indicatorColor;
  final Color selectedIndicatorColor;

  OnBoardingScreen({
    this.indicatorColor = Colors.grey,
    this.selectedIndicatorColor = Colors.black,
  });

  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    Provider.of<OnBoardingProvider>(context, listen: false)
        .initBoardingList(context);

    double _height = MediaQuery.of(context).size.height;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      body: Stack(
        clipBehavior: Clip.none,
        children: [
          Provider.of<ThemeProvider>(context).darkTheme
              ? SizedBox()
              : Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Image.asset(Images.background, fit: BoxFit.fill),
                ),
          Consumer<OnBoardingProvider>(
            builder: (context, onBoardingList, child) => ListView(
              children: [
                SizedBox(
                  height: _height * 0.75,
                  child: PageView.builder(
                    itemCount: onBoardingList.onBoardingList.length,
                    controller: _pageController,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding:
                            EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                                onBoardingList.onBoardingList[index].imageUrl,
                                height: _height * 0.5),
                            Text(
                              onBoardingList.onBoardingList[index].title,
                              style: titilliumBold.copyWith(
                                  fontSize:
                                      _height * (index == 0 ? 0.025 : 0.020)),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              onBoardingList.onBoardingList[index].description,
                              textAlign: TextAlign.center,
                              style: titilliumRegular.copyWith(
                                fontSize: _height * 0.017,
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    onPageChanged: (index) {
                      onBoardingList.changeSelectIndex(index);
                    },
                  ),
                ),
                Column(
                  children: [
                    SizedBox(height: 50),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _pageIndicators(
                            onBoardingList.onBoardingList, context),
                      ),
                    ),
                    Container(
                       height: Responsive.isMobile(context)?45:55,
                      margin: EdgeInsets.symmetric(
                          horizontal: 70,
                          vertical: Dimensions.PADDING_SIZE_SMALL),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          gradient: LinearGradient(colors: [
                            Theme.of(context).primaryColor,
                            Theme.of(context).primaryColor,
                            Theme.of(context).primaryColor,
                          ])),
                      child: TextButton(
                        onPressed: () {
                          if (Provider.of<OnBoardingProvider>(context,
                                      listen: false)
                                  .selectedIndex ==
                              onBoardingList.onBoardingList.length - 1) {
                            Provider.of<SplashProvider>(context, listen: false)
                                .disableIntro();
                            // Navigator.of(context).pushReplacement(
                            //   MaterialPageRoute(
                            //     builder: (context) => AuthScreen(),
                            //   ),
                            // );
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                builder: (context) => DashBoardScreen(),
                              ),
                            );
                          } else {
                            _pageController.animateToPage(
                                Provider.of<OnBoardingProvider>(context,
                                            listen: false)
                                        .selectedIndex +
                                    1,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInOut);
                          }
                        },
                        child: Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                              onBoardingList.selectedIndex ==
                                      onBoardingList.onBoardingList.length - 1
                                  ? getTranslated('SHOP_NOW', context)
                                  : getTranslated('NEXT', context),
                              style: titilliumSemiBold.copyWith(
                                  color: Colors.white,
                                  fontSize: Dimensions.FONT_SIZE_LARGE)),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            top: 60,
            left: 20,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: Dimensions.PADDING_SIZE_SMALL,
              ),
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.circular(
                  Dimensions.PADDING_SIZE_DEFAULT,
                ),
              ),
              child: InkWell(
                onTap: () {
                  showAnimatedDialog(
                      context, CurrencyDialog(isCurrency: false));
                },
                child: Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    getTranslated("choose_language", context),
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 60,
            right: 20,
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_SMALL),
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.circular(
                  Dimensions.PADDING_SIZE_DEFAULT,
                ),
              ),
              child: InkWell(
                onTap: () {
                  Provider.of<SplashProvider>(context, listen: false)
                      .disableIntro();
                  // Navigator.of(context).pushReplacement(
                  //   MaterialPageRoute(
                  //     builder: (context) => AuthScreen(),
                  //   ),
                  // );
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => DashBoardScreen(),
                    ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: Text(
                    getTranslated("SKIP", context),
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> _pageIndicators(var onBoardingList, BuildContext context) {
    List<Container> _indicators = [];

    for (int i = 0; i < onBoardingList.length; i++) {
      _indicators.add(
        Container(
          width: i == Provider.of<OnBoardingProvider>(context).selectedIndex
              ? 18
              : 7,
          height: 7,
          margin: EdgeInsets.only(right: 5),
          decoration: BoxDecoration(
            color: i == Provider.of<OnBoardingProvider>(context).selectedIndex
                ? Theme.of(context).primaryColor
                : Colors.white,
            borderRadius:
                i == Provider.of<OnBoardingProvider>(context).selectedIndex
                    ? BorderRadius.circular(50)
                    : BorderRadius.circular(25),
          ),
        ),
      );
    }
    return _indicators;
  }
}
