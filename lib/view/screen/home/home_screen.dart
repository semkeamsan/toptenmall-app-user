import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_sixvalley_ecommerce/helper/product_type.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/banner_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/brand_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/featured_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/flash_deal_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/home_category_product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/top_seller_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/title_row.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/banners_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/best_sell_product_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/category_round_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/products_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/widget/videos_banner_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/view_all_product_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/promotion/promotion_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/promotion/widgets/promotion_product_view.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/search_screen.dart';
import 'package:provider/provider.dart';
import 'dart:ui';

import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ScrollController _scrollController = ScrollController();
  Future<void> _loadData(BuildContext context, bool reload) async {
    await Provider.of<BannerProvider>(context, listen: false)
        .getVideoList(reload, context);

    await Provider.of<BannerProvider>(context, listen: false)
        .getBannerList(reload, context);

    await Provider.of<BannerProvider>(context, listen: false)
        .getFooterBannerList(context);
    await Provider.of<CategoryProvider>(context, listen: false)
        .getCategoryList(reload, context);
    await Provider.of<HomeCategoryProductProvider>(context, listen: false)
        .getHomeCategoryProductList(reload, context);
    await Provider.of<TopSellerProvider>(context, listen: false)
        .getTopSellerList(reload, context);
    //await Provider.of<FlashDealProvider>(context, listen: false).getMegaDealList(reload, context,_languageCode,true);
    await Provider.of<BrandProvider>(context, listen: false)
        .getBrandList(reload, context);
    await Provider.of<ProductProvider>(context, listen: false)
        .getLatestProductList(1, context, reload: reload);
    await Provider.of<ProductProvider>(context, listen: false)
        .getFeaturedProductList('1', context, reload: reload);
    await Provider.of<FeaturedDealProvider>(context, listen: false)
        .getFeaturedDealList(reload, context);
    await Provider.of<ProductProvider>(context, listen: false)
        .getLProductList('1', context, reload: reload);
  }

  void passData(int index, String title) {
    index = index;
    title = title;
  }

  @override
  void initState() {
    super.initState();
    Provider.of<FlashDealProvider>(context, listen: false)
        .getMegaDealList(true, context, true);

    _loadData(context, false);

    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn()) {
      Provider.of<CartProvider>(context, listen: false).uploadToServer(context);
      Provider.of<CartProvider>(context, listen: false).getCartDataAPI(context);
    } else {
      Provider.of<CartProvider>(context, listen: false).getCartData();
    }
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> types = [
      getTranslated('new_arrival', context),
      getTranslated('top_product', context),
      getTranslated('best_selling', context)
    ];
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      resizeToAvoidBottomInset: false,
      floatingActionButton: new FloatingActionButton(
        onPressed: () async {
          // try {
          //   ///checks if the app is installed on your mobile device
          //   bool isInstalled =
          //       await DeviceApps.isAppInstalled('com.toptenmallseller.app');
          //   if (isInstalled) {
          //     DeviceApps.openApp("com.toptenmallseller.app");
          //   } else {
          //     ///if the app is not installed it lunches google play store so you can install it from there
          //     launch("market://details?id=" + "com.toptenmallseller.app");
          //   }
          // } catch (e) {
          //   print(e);
          // }

          _launchUrl(AppConstants.REGISTER_VEDOR);
        },
        child: const Icon(Icons.add),
        backgroundColor: ColorResources.getPrimary(context),
      ),
      body: SafeArea(
        child: RefreshIndicator(
          color: Colors.white,
          backgroundColor: Theme.of(context).primaryColor,
          onRefresh: () async {
            await _loadData(context, true);
            await Provider.of<FlashDealProvider>(context, listen: false)
                .getMegaDealList(true, context, false);

            return true;
          },
          child: CustomScrollView(
            controller: _scrollController,
            slivers: [
              // App Bar
              SliverPadding(
                padding: const EdgeInsets.only(
                  top: Dimensions.PADDING_SIZE_EXTRA_LARGE,
                  bottom: Dimensions.PADDING_SIZE_SMALL,
                ),
                sliver: SliverAppBar(
                  pinned: false,
                  stretch: false,
                  floating: false,
                  elevation: 2,
                  centerTitle: false,
                  automaticallyImplyLeading: false,
                  backgroundColor: Theme.of(context).primaryColor,
                  title: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => SearchScreen(),
                          ),
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 2.0),
                          color: Theme.of(context).primaryColor,
                          alignment: Alignment.center,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Dimensions.PADDING_SIZE_SMALL,
                              vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
                            ),
                            width: MediaQuery.of(context).size.width - 80.0,
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  Dimensions.PADDING_SIZE_EXTRA_LARGE),
                              color: Theme.of(context).primaryColor,
                              boxShadow: [
                                BoxShadow(
                                    color: ColorResources.getYellow(context),
                                    spreadRadius: 1,
                                    blurRadius: .5)
                              ],
                            ),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    getTranslated("SEARCH_NOW", context),
                                    style: titilliumRegular.copyWith(
                                      color: Theme.of(context).hintColor,
                                      fontSize: Responsive.isMobile(context)
                                          ? Dimensions.FONT_SIZE_SMALL
                                          : Dimensions.FONT_SIZE_SMALL_IPAD,
                                    ),
                                  ),
                                  Icon(
                                    Icons.search,
                                    color: ColorResources.getYellow(context),
                                    size: Responsive.isMobile(context)
                                        ? Dimensions.ICON_SIZE_LARGE
                                        : Dimensions.ICON_SIZE_LARGE_IPAD,
                                  ),
                                ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => CartScreen()));
                      },
                      icon: Stack(clipBehavior: Clip.none, children: [
                        Image.asset(
                          Images.cart_arrow_down_image,
                          height: Responsive.isMobile(context)
                              ? Dimensions.ICON_SIZE_DEFAULT
                              : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                          width: Responsive.isMobile(context)
                              ? Dimensions.ICON_SIZE_DEFAULT
                              : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                          color: Colors.white,
                        ),
                        Positioned(
                          top: -4,
                          right: -4,
                          child: Consumer<CartProvider>(
                              builder: (context, cart, child) {
                            return CircleAvatar(
                              radius: 7,
                              backgroundColor: ColorResources.RED,
                              child: Text(cart.cartList.length.toString(),
                                  style: titilliumSemiBold.copyWith(
                                    color: ColorResources.WHITE,
                                    fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                                  )),
                            );
                          }),
                        ),
                      ]),
                    ),
                  ],
                ),
              ),

              SliverToBoxAdapter(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                      topRight: Radius.circular(Dimensions.PADDING_SIZE_SMALL),
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            top: Responsive.isMobile(context)
                                ? Dimensions.PADDING_SIZE_SMALL
                                : Dimensions.PADDING_SIZE_SMALL_IPAD),
                        child: VideoBannerView(),
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(top: Dimensions.PADDING_SIZE_LARGE),
                        child: BannersView(),
                      ),

                      // Category
                      // Padding(

                      //   padding: EdgeInsets.fromLTRB(
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       0,
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       Dimensions.PADDING_SIZE_SMALL),
                      //   child: Row(
                      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //     children: [
                      //       Container(
                      //         width: MediaQuery.of(context).size.width - 50,
                      //         child: CategoryList(isHomePage: true),
                      //       ),
                      //       InkWell(
                      //           onTap: () {
                      //             Navigator.push(
                      //               context,
                      //               MaterialPageRoute(
                      //                 builder: (_) => AllCategoryScreen(),
                      //               ),
                      //             );
                      //           },
                      //           child: Icon(Icons.menu))
                      //     ],
                      //   ),
                      // ),

                      //Category Round
                      Padding(
                        padding:
                            EdgeInsets.only(left: 10.0, top: 20.0, right: 10.0),
                        child: CategoryRoundView(isHomePage: true),
                      ),

                      // Flash Deals
                      Consumer<FlashDealProvider>(
                        builder: (context, flashDealProvider, child) {
                          if (flashDealProvider.flashDeal == null ||
                              flashDealProvider.flashDeal.title == null ||
                              flashDealProvider.flashDealList.length == 0) {
                            return SizedBox.shrink();
                          }

                          return Container(
                            child: Column(children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(
                                    Dimensions.PADDING_SIZE_SMALL,
                                    20,
                                    Dimensions.PADDING_SIZE_SMALL,
                                    Dimensions.PADDING_SIZE_SMALL),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          getTranslated("Flash", context)
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.w900,
                                            fontSize: Responsive.isMobile(
                                                    context)
                                                ? Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE
                                                : Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE_IPAD,
                                          ),
                                        ),
                                        Image.asset(
                                          "assets/images/lightning.png",
                                          width: 20,
                                        ),
                                        Text(
                                          getTranslated("Sale", context)
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.w900,
                                            fontSize: Responsive.isMobile(
                                                    context)
                                                ? Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE
                                                : Dimensions
                                                    .FONT_SIZE_EXTRA_LARGE_IPAD,
                                          ),
                                        ),
                                      ],
                                    ),
                                    flashDealProvider.flashDealList.length > 6
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      PageRouteBuilder(
                                                        transitionDuration:
                                                            Duration(
                                                                milliseconds:
                                                                    1000),
                                                        pageBuilder: (context,
                                                                anim1, anim2) =>
                                                            PromotionScreen(),
                                                      ));
                                                },
                                                child: Row(children: [
                                                  Text(
                                                      getTranslated(
                                                          "VIEW_ALL", context),
                                                      style: titilliumRegular
                                                          .copyWith(
                                                        color: Theme.of(context)
                                                            .primaryColor,
                                                        fontSize: Responsive
                                                                .isMobile(
                                                                    context)
                                                            ? Dimensions
                                                                .FONT_SIZE_SMALL
                                                            : Dimensions
                                                                .FONT_SIZE_SMALL_IPAD,
                                                      )),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: Dimensions
                                                            .PADDING_SIZE_SMALL,
                                                        top: Dimensions
                                                            .PADDING_SIZE_SMALL,
                                                        bottom: Dimensions
                                                            .PADDING_SIZE_SMALL),
                                                    child: Icon(
                                                      Icons.arrow_forward_ios,
                                                      color: Theme.of(context)
                                                          .hintColor,
                                                      size: Dimensions
                                                          .FONT_SIZE_SMALL,
                                                    ),
                                                  ),
                                                ]),
                                              ),
                                            ],
                                          )
                                        : SizedBox.shrink(),
                                  ],
                                ),
                              ),

                              //flash_deal

                              Padding(
                                padding:
                                    EdgeInsets.only(left: 10.0, right: 10.0),
                                child: PromotionProductView(isHomePage: true),
                              ),
                            ]),
                          );
                        },
                      ),

                      Consumer<ProductProvider>(
                        builder: (context, productProvider, child) {
                          return productProvider.latestProductList.length != 0
                              ? Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          Dimensions.PADDING_SIZE_SMALL,
                                          10,
                                          Dimensions.PADDING_SIZE_SMALL,
                                          Dimensions.PADDING_SIZE_SMALL),
                                      child: TitleRow(
                                          title: getTranslated(
                                              'best_seller', context),
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              PageRouteBuilder(
                                                transitionDuration: Duration(
                                                    milliseconds: 1000),
                                                pageBuilder:
                                                    (context, anim1, anim2) =>
                                                        AllProductScreen(
                                                  productType:
                                                      ProductType.BEST_SELLING,
                                                ),
                                              ),
                                            );
                                          }),
                                    ),

                                    //Best Sell
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 10.0, right: 10.0),
                                      child:
                                          BestSellProductView(isHomePage: true),
                                    ),
                                  ],
                                )
                              : SizedBox.shrink();
                        },
                      ),

                      // Consumer<FlashDealProvider>(
                      //   builder: (context, megaDeal, child) {
                      //     print(megaDeal.flashDealList.length);
                      //     return megaDeal.flashDeal == null
                      //         ? Padding(
                      //             padding: EdgeInsets.symmetric(
                      //                 horizontal:
                      //                     Dimensions.PADDING_SIZE_SMALL),
                      //             child: Container(
                      //                 height: 150, child: FlashDealsView()),
                      //           )
                      //         : (megaDeal.flashDeal.id != null &&
                      //                 megaDeal.flashDealList != null &&
                      //                 megaDeal.flashDealList.length > 0)
                      //             ? Padding(
                      //                 padding: EdgeInsets.symmetric(
                      //                     horizontal:
                      //                         Dimensions.PADDING_SIZE_SMALL),
                      //                 child: Container(
                      //                     height: 150, child: FlashDealsView()),
                      //               )
                      //             : SizedBox.shrink();
                      //   },
                      // ),

                      // Featured Products
                      // Padding(
                      //   padding: EdgeInsets.fromLTRB(
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       50,
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       Dimensions.PADDING_SIZE_SMALL),
                      //   child: TitleRow(
                      //       title: getTranslated('featured_products', context),
                      //       onTap: () {
                      //         Navigator.push(
                      //             context,
                      //             MaterialPageRoute(
                      //                 builder: (_) => AllProductScreen(
                      //                     productType:
                      //                         ProductType.FEATURED_PRODUCT)));
                      //       }),
                      // ),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: Dimensions.PADDING_SIZE_SMALL),
                      //   child: FeaturedProductView(
                      //     scrollController: _scrollController,
                      //     isHome: true,
                      //   ),
                      // ),

                      //Featured Deal
                      // Consumer<FeaturedDealProvider>(
                      //   builder: (context, featuredDealProvider, child) {
                      //     return featuredDealProvider.featuredDealList == null
                      //         ? Padding(
                      //             padding: EdgeInsets.fromLTRB(
                      //                 Dimensions.PADDING_SIZE_SMALL,
                      //                 20,
                      //                 Dimensions.PADDING_SIZE_SMALL,
                      //                 Dimensions.PADDING_SIZE_SMALL),
                      //             child: TitleRow(
                      //                 title: getTranslated(
                      //                     'featured_deals', context),
                      //                 onTap: () {
                      //                   Navigator.push(
                      //                       context,
                      //                       MaterialPageRoute(
                      //                           builder: (_) =>
                      //                               FeaturedDealScreen()));
                      //                 }),
                      //           )
                      //         : (featuredDealProvider.featuredDealList !=
                      //                     null &&
                      //                 featuredDealProvider
                      //                         .featuredDealList.length >
                      //                     0)
                      //             ? Padding(
                      //                 padding: EdgeInsets.fromLTRB(
                      //                     Dimensions.PADDING_SIZE_SMALL,
                      //                     20,
                      //                     Dimensions.PADDING_SIZE_SMALL,
                      //                     Dimensions.PADDING_SIZE_SMALL),
                      //                 child: TitleRow(
                      //                     title: getTranslated(
                      //                         'featured_deals', context),
                      //                     onTap: () {
                      //                       Navigator.push(
                      //                           context,
                      //                           MaterialPageRoute(
                      //                               builder: (_) =>
                      //                                   FeaturedDealScreen()));
                      //                     }),
                      //               )
                      //             : SizedBox.shrink();
                      //   },
                      // ),
                      // Consumer<FeaturedDealProvider>(
                      //   builder: (context, featuredDeal, child) {
                      //     return featuredDeal.featuredDealList == null
                      //         ? Padding(
                      //             padding: EdgeInsets.symmetric(
                      //                 horizontal:
                      //                     Dimensions.PADDING_SIZE_SMALL),
                      //             child: Container(
                      //                 height: 150, child: FeaturedDealsView()),
                      //           )
                      //         : (featuredDeal.featuredDealList != null &&
                      //                 featuredDeal.featuredDealList.length > 0)
                      //             ? Padding(
                      //                 padding: EdgeInsets.symmetric(
                      //                     horizontal:
                      //                         Dimensions.PADDING_SIZE_SMALL),
                      //                 child: Container(
                      //                     height: 150,
                      //                     child: FeaturedDealsView()),
                      //               )
                      //             : SizedBox.shrink();
                      //   },
                      // ),

                      // // Latest Products
                      // Padding(
                      //   padding: EdgeInsets.fromLTRB(
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       20,
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       Dimensions.PADDING_SIZE_SMALL),
                      //   child: TitleRow(
                      //       title: getTranslated('latest_products', context),
                      //       onTap: () {
                      //         Navigator.push(
                      //             context,
                      //             MaterialPageRoute(
                      //                 builder: (_) => AllProductScreen(
                      //                     productType:
                      //                         ProductType.LATEST_PRODUCT)));
                      //       }),
                      // ),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: Dimensions.PADDING_SIZE_SMALL),
                      //   child: LatestProductView(
                      //       scrollController: _scrollController),
                      // ),

                      //Home category fashion

                      // Padding(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: Dimensions.PADDING_SIZE_SMALL),
                      //   child: HomeCategoryProductView(isHomePage: true),
                      // ),

                      //top seller
                      // Padding(
                      //   padding: EdgeInsets.fromLTRB(
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       20,
                      //       Dimensions.PADDING_SIZE_SMALL,
                      //       Dimensions.PADDING_SIZE_SMALL),
                      //   child: TitleRow(
                      //     title: getTranslated('top_seller', context),
                      //     onTap: () {
                      //       Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //               builder: (_) => AllTopSellerScreen(
                      //                     topSeller: null,
                      //                   )));
                      //     },
                      //   ),
                      // ),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: Dimensions.PADDING_SIZE_SMALL),
                      //   child: TopSellerView(isHomePage: true),
                      // ),

                      Consumer<ProductProvider>(
                          builder: (ctx, prodProvider, child) {
                        return Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 15, 0, 5),
                              child: Consumer<ProductProvider>(
                                  builder: (ctx, prodProvider, child) {
                                return Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            right: Dimensions
                                                .PADDING_SIZE_DEFAULT),
                                        child: Text(
                                            prodProvider.title == 'xyz'
                                                ? getTranslated(
                                                    'explore_more_products',
                                                    context)
                                                : prodProvider.title,
                                            style: robotoBold.copyWith(
                                              fontSize:
                                                  Responsive.isMobile(context)
                                                      ? Dimensions
                                                          .FONT_SIZE_SMALL
                                                      : Dimensions
                                                          .FONT_SIZE_SMALL_IPAD,
                                            )),
                                      ),
                                    ),
                                    prodProvider.latestProductList != null
                                        ? PopupMenuButton(
                                            itemBuilder: (context) {
                                              return [
                                                PopupMenuItem(
                                                    value:
                                                        ProductType.NEW_ARRIVAL,
                                                    child: Text(getTranslated(
                                                        'new_arrival',
                                                        context)),
                                                    textStyle:
                                                        robotoRegular.copyWith(
                                                      color: Colors.black,
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_SMALL
                                                          : Dimensions
                                                              .FONT_SIZE_SMALL_IPAD,
                                                    )),
                                                PopupMenuItem(
                                                    value:
                                                        ProductType.TOP_PRODUCT,
                                                    child: Text(getTranslated(
                                                        'top_product',
                                                        context)),
                                                    textStyle:
                                                        robotoRegular.copyWith(
                                                      color: Colors.black,
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_SMALL
                                                          : Dimensions
                                                              .FONT_SIZE_SMALL_IPAD,
                                                    )),
                                                PopupMenuItem(
                                                    value: ProductType
                                                        .BEST_SELLING,
                                                    child: Text(getTranslated(
                                                        'best_selling',
                                                        context)),
                                                    textStyle:
                                                        robotoRegular.copyWith(
                                                      color: Colors.black,
                                                      fontSize: Responsive
                                                              .isMobile(context)
                                                          ? Dimensions
                                                              .FONT_SIZE_SMALL
                                                          : Dimensions
                                                              .FONT_SIZE_SMALL_IPAD,
                                                    )),
                                              ];
                                            },
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius
                                                    .circular(Dimensions
                                                        .PADDING_SIZE_SMALL)),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: Dimensions
                                                      .PADDING_SIZE_SMALL),
                                              child: Icon(
                                                Icons.filter_list,
                                                size: Responsive.isMobile(
                                                        context)
                                                    ? Dimensions.FONT_SIZE_SMALL
                                                    : Dimensions
                                                        .FONT_SIZE_SMALL_IPAD,
                                              ),
                                            ),
                                            onSelected: (value) {
                                              if (value ==
                                                  ProductType.NEW_ARRIVAL) {
                                                Provider.of<ProductProvider>(
                                                        context,
                                                        listen: false)
                                                    .changeTypeOfProduct(
                                                        value, types[0]);
                                              } else if (value ==
                                                  ProductType.TOP_PRODUCT) {
                                                Provider.of<ProductProvider>(
                                                        context,
                                                        listen: false)
                                                    .changeTypeOfProduct(
                                                        value, types[1]);
                                              } else if (value ==
                                                  ProductType.BEST_SELLING) {
                                                Provider.of<ProductProvider>(
                                                        context,
                                                        listen: false)
                                                    .changeTypeOfProduct(
                                                        value, types[2]);
                                              }

                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: Dimensions
                                                        .PADDING_SIZE_SMALL),
                                                child: ProductView(
                                                    isHomePage: false,
                                                    productType: value,
                                                    scrollController:
                                                        _scrollController),
                                              );
                                              Provider.of<ProductProvider>(
                                                      context,
                                                      listen: false)
                                                  .getLatestProductList(
                                                      1, context,
                                                      reload: true);
                                            })
                                        : SizedBox(),
                                  ],
                                );
                              }),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: Dimensions.PADDING_SIZE_SMALL),
                              child: ProductView(
                                  isHomePage: false,
                                  productType: ProductType.NEW_ARRIVAL,
                                  scrollController: _scrollController),
                            ),
                          ],
                        );
                      }),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SliverDelegate extends SliverPersistentHeaderDelegate {
  Widget child;
  SliverDelegate({@required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => 50;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverDelegate oldDelegate) {
    return oldDelegate.maxExtent != 50 ||
        oldDelegate.minExtent != 50 ||
        child != oldDelegate.child;
  }
}
