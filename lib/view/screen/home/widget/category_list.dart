import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/brand_and_category_product_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class CategoryList extends StatelessWidget {
  final bool isHomePage;
  CategoryList({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<CategoryProvider>(
      builder: (context, categoryProvider, child) {
        return Container(
          height: 50.0,
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            itemCount: categoryProvider.categoryList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => BrandAndCategoryProductScreen(
                        isBrand: false,
                        id: categoryProvider.categoryList[index].id.toString(),
                        name: categoryProvider.categoryList[index].name,
                      ),
                    ),
                  );
                },
                child: Container(
                  padding:
                      const EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  child: new Row(
                    children: [
                      new Row(children: [
                        Text(
                          categoryProvider.categoryList[index].name != ''
                              ? categoryProvider.categoryList[index].name
                              : getTranslated('CATEGORY', context),
                        )
                      ])
                    ],
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}

Widget CategoryStyleItems(BuildContext context,
    {CategoryProvider categoryProvider, int index}) {
  return InkWell(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => BrandAndCategoryProductScreen(
            isBrand: false,
            id: categoryProvider.categoryList[index].id.toString(),
            name: categoryProvider.categoryList[index].name,
          ),
        ),
      );
    },
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                //color: ColorResources.getTextBg(context),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
              ),
              alignment: Alignment.center,
              child: Container(
                child: Text(
                  categoryProvider.categoryList.length != 0
                      ? categoryProvider.categoryList[index].name
                      : getTranslated('CATEGORY', context),
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  //overflow: TextOverflow.ellipsis,
                  style: titilliumSemiBold.copyWith(
                      color: Provider.of<ThemeProvider>(context).darkTheme
                          ? Colors.black
                          : Colors.grey,
                      fontSize: Dimensions.FONT_SIZE_SMALL),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

class CategoryShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 5,
        childAspectRatio: (1 / 1),
      ),
      itemCount: 5,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: Colors.grey[
                    Provider.of<ThemeProvider>(context).darkTheme ? 700 : 200],
                spreadRadius: 2,
                blurRadius: 5)
          ]),
          margin: EdgeInsets.all(3),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              flex: 7,
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: Provider.of<CategoryProvider>(context)
                        .categoryList
                        .length ==
                    0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: ColorResources.getTextBg(context),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                  child: Shimmer.fromColors(
                    //baseColor: Colors.grey[300],
                    //highlightColor: Colors.grey[100],
                    enabled: Provider.of<CategoryProvider>(context)
                            .categoryList
                            .length ==
                        0,
                    child: Container(
                      height: 10,
                      color: Colors.white,
                      margin: EdgeInsets.only(
                        left: 15,
                        right: 15,
                      ),
                    ),
                  ),
                )),
          ]),
        );
      },
    );
  }
}
