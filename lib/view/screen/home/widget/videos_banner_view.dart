import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/provider/banner_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoBannerView extends StatelessWidget {
  const VideoBannerView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Consumer<BannerProvider>(
          builder: (context, bannerProvider, child) {
            double _width = MediaQuery.of(context).size.width;
            return Container(
              width: _width,
             // height: _width * .6,
              child: bannerProvider.videoList != null
                  ? bannerProvider.videoList.length != 0
                      ? Column(
                          children: [
                            CarouselSlider.builder(
                              options: CarouselOptions(
                                viewportFraction: .95,
                                autoPlay: false,
                                enlargeCenterPage: true,
                                disableCenter: true,
                                onPageChanged: (index, reason) {
                                  Provider.of<BannerProvider>(context,
                                          listen: false)
                                      .setVideoCurrentIndex(index);
                                },
                              ),
                              itemCount: bannerProvider.videoList.length == 0
                                  ? 1
                                  : bannerProvider.videoList.length,
                              itemBuilder: (context, index, _) {
                                // YT Controller
                                final _ytController = YoutubePlayerController(
                                  //initialVideoId: YoutubePlayer.convertUrlToId( bannerProvider.videoList[index].url),
                                  initialVideoId: YoutubePlayer.convertUrlToId(
                                      bannerProvider.videoList[index].videoId),
                                  flags: const YoutubePlayerFlags(
                                    autoPlay: false,
                                    enableCaption: true,
                                    captionLanguage: 'en',
                                    disableDragSeek: true,
                                  ),
                                );

                                // for container visibility
                                bool _isPlaying = false;

                                return ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: YoutubePlayer(
                                    controller: _ytController
                                      ..addListener(() {
                                        if (_ytController.value.isPlaying) {
                                          _isPlaying = true;
                                        } else {
                                          _isPlaying = false;
                                        }
                                      }),
                                    showVideoProgressIndicator: true,
                                    progressIndicatorColor:
                                        ColorResources.getPrimary(context),
                                    bottomActions: [
                                      CurrentPosition(),
                                      ProgressBar(isExpanded: true),
                                      //FullScreenButton(),
                                    ],
                                    onEnded: (YoutubeMetaData _md) {
                                      ///---------------------------   ISSUE NO. 2
                                      _ytController.reset();
                                      // _ytController.seekTo(const Duration(seconds: 1));
                                      // _ytController.pause();
                                      _md.videoId;
                                      print(_md.title);
                                    },
                                  ),
                                );
                              },
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: Dimensions.PADDING_SIZE_DEFAULT),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:
                                    bannerProvider.videoList.map((banner) {
                                  int index =
                                      bannerProvider.videoList.indexOf(banner);
                                  return TabPageSelectorIndicator(
                                    backgroundColor: index ==
                                            bannerProvider.videocurrentIndex
                                        ? Theme.of(context).primaryColor
                                        : Colors.grey,
                                    borderColor: index ==
                                            bannerProvider.videocurrentIndex
                                        ? Theme.of(context).primaryColor
                                        : Colors.grey,
                                    size: 10,
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        )
                      : SizedBox()
                  : Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      enabled: bannerProvider.mainBannerList == null,
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: ColorResources.WHITE,
                          )),
                    ),
            );
          },
        ),
      ],
    );
  }
}
