import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/product_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/product_details_screen.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class BestSellProductView extends StatelessWidget {
  final bool isHomePage;
  BestSellProductView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<ProductProvider>(
      builder: (context, productProvider, child) {
        return productProvider.latestProductList.length != 0
            ? StaggeredGridView.countBuilder(
                itemCount: isHomePage
                    ? productProvider.latestProductList.length > 6
                        ? 6
                        : productProvider.latestProductList.length
                    : productProvider.latestProductList.length,
                crossAxisCount: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                addAutomaticKeepAlives: true,
                padding: EdgeInsets.all(0),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Product productModel =
                          productProvider.latestProductList[index];
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          transitionDuration: Duration(milliseconds: 1000),
                          pageBuilder: (context, anim1, anim2) =>
                              ProductDetails(product: productModel),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey[300],
                        ),
                        borderRadius: BorderRadius.circular(
                          Dimensions.PADDING_SIZE_DEFAULT,
                        ),
                        color: Theme.of(context).highlightColor,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(
                                  Dimensions.PADDING_SIZE_DEFAULT),
                              topRight: Radius.circular(
                                  Dimensions.PADDING_SIZE_DEFAULT),
                            ),
                            child: CachedNetworkImage(
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.cover,
                              imageUrl: Provider.of<SplashProvider>(context,
                                          listen: false)
                                      .baseUrls
                                      .productThumbnailUrl +
                                  '/' +
                                  productProvider
                                      .latestProductList[index].thumbnail,
                              placeholder: (c, o) => Image.asset(
                                Images.placeholder,
                                width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Container(
                            child: Text(
                              PriceConverter.convertPrice(
                                  context,
                                  productProvider
                                          .latestProductList[index].unitPrice ??
                                      0.0,
                                  discountType: productProvider
                                      .latestProductList[index].discountType,
                                  discount: productProvider
                                      .latestProductList[index].discount),
                              style: robotoBold.copyWith(
                                color: ColorResources.getRed(context),
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                              ),
                            ),
                          ),
                          SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                          Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Dimensions.PADDING_SIZE_DEFAULT,
                              vertical: Dimensions.PADDING_SIZE_SMALL,
                            ),
                            child: Text(
                              productProvider.latestProductList[index].name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: robotoRegular.copyWith(
                                color: Theme.of(context).hintColor,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )
            : BrandShimmer(isHomePage: isHomePage);
      },
    );
  }
}

class BrandShimmer extends StatelessWidget {
  final bool isHomePage;
  BrandShimmer({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: (1 / 1.3),
        mainAxisSpacing: 10,
        crossAxisSpacing: 3,
      ),
      itemCount: isHomePage ? 6 : 30,
      shrinkWrap: true,
      physics: isHomePage ? NeverScrollableScrollPhysics() : null,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled:
              Provider.of<ProductProvider>(context).latestProductList.length ==
                  0,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(color: ColorResources.WHITE),
              ),
            ),
          ]),
        );
      },
    );
  }
}
