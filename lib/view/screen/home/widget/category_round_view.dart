import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/brand_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/category_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/brand_and_category_product_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class CategoryRoundView extends StatelessWidget {
  final bool isHomePage;
  CategoryRoundView({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Consumer<CategoryProvider>(
      builder: (context, categoryProvider, child) {
        return categoryProvider.categoryList.length != 0
            ? GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5,
                  childAspectRatio: (1 / 1.3),
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 5,
                ),
                itemCount: categoryProvider.categoryList.length != 0
                    ? isHomePage
                        ? categoryProvider.categoryList.length > 10
                            ? 10
                            : categoryProvider.categoryList.length
                        : categoryProvider.categoryList.length
                    : 8,
                shrinkWrap: true,
                physics: isHomePage
                    ? NeverScrollableScrollPhysics()
                    : BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => BrandAndCategoryProductScreen(
                            isBrand: false,
                            id: categoryProvider.categoryList[index].id
                                .toString(),
                            name: categoryProvider.categoryList[index].name,
                            image: categoryProvider.categoryList[index].icon,
                          ),
                        ),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Expanded(
                          child: CircleAvatar(
                            backgroundImage: AssetImage(Images.placeholder),
                            foregroundImage: CachedNetworkImageProvider(
                                Provider.of<SplashProvider>(context,
                                            listen: false)
                                        .baseUrls
                                        .categoryImageUrl +
                                    '/' +
                                    categoryProvider.categoryList[index].icon),
                          ),
                        ),
                        SizedBox(
                          //height: (MediaQuery.of(context).size.width / 5) * 0.3,
                          child: Center(
                              child: Text(
                            categoryProvider.categoryList[index].name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            style: titilliumRegular.copyWith(
                              color:
                                  Provider.of<ThemeProvider>(context).darkTheme
                                      ? Colors.black
                                      : Colors.grey,
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_SMALL
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                          )),
                        ),
                      ],
                    ),
                  );
                },
              )
            : BrandShimmer(isHomePage: isHomePage);
      },
    );
  }
}

class BrandShimmer extends StatelessWidget {
  final bool isHomePage;
  BrandShimmer({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 5,
        childAspectRatio: (1 / 1.3),
        mainAxisSpacing: 10,
        crossAxisSpacing: 5,
      ),
      itemCount: isHomePage ? 10 : 30,
      shrinkWrap: true,
      physics: isHomePage ? NeverScrollableScrollPhysics() : null,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          enabled:
              Provider.of<CategoryProvider>(context).categoryList.length == 0,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: ColorResources.WHITE,
                  shape: BoxShape.circle,
                ),
              ),
            ),
            Container(
                height: 10,
                color: ColorResources.WHITE,
                margin: EdgeInsets.only(left: 25, right: 25)),
          ]),
        );
      },
    );
  }
}
