import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/custom_app_bar.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:provider/provider.dart';

class FaqScreen extends StatefulWidget {
  final String title;
  FaqScreen({@required this.title});

  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {
  bool isExpanded = false;
  int indexOfExpanded = 0;
  @override
  Widget build(BuildContext context) {
    print(Provider.of<SplashProvider>(context).configModel.faq);
    return Scaffold(
      body: Column(
        children: [
          CustomAppBar(title: widget.title),
          Provider.of<SplashProvider>(context).configModel.faq != null &&
                  Provider.of<SplashProvider>(context).configModel.faq.length >
                      0
              ? Expanded(
                  child: ListView.builder(
                      itemCount: Provider.of<SplashProvider>(context)
                          .configModel
                          .faq
                          .length,
                      itemBuilder: (ctx, index) {
                        return Consumer<SplashProvider>(
                          builder: (ctx, faq, child) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Flexible(
                                        child: ExpansionTile(
                                      onExpansionChanged: (bool isShow) {
                                        indexOfExpanded = index;
                                        isExpanded = isShow;
                                        setState(() {});
                                      },
                                      iconColor: Theme.of(context).primaryColor,
                                      title: Text(
                                        faq.configModel.faq[index].question,
                                        style: robotoBold.copyWith(
                                          color: ColorResources.getTextTitle(
                                              context),
                                          fontSize: Responsive.isMobile(context)
                                              ? Dimensions.FONT_SIZE_LARGE
                                              : Dimensions.FONT_SIZE_LARGE_IPAD,
                                        ),
                                      ),
                                      leading: Icon(
                                        Icons.collections_bookmark_outlined,
                                        color: ColorResources.getTextTitle(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                              : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      trailing: Icon(
                                        (indexOfExpanded == index && isExpanded)
                                            ? Icons.keyboard_arrow_up
                                            : Icons.keyboard_arrow_down,
                                        color: ColorResources.getTextTitle(
                                            context),
                                        size: Responsive.isMobile(context)
                                            ? Dimensions.ICON_SIZE_DEFAULT
                                              : Dimensions.ICON_SIZE_DEFAULT_IPAD,
                                      ),
                                      childrenPadding: EdgeInsets.symmetric(
                                        vertical: Responsive.isMobile(context)
                                            ? 20.0
                                            : 20.0,
                                        horizontal: Responsive.isMobile(context)
                                            ? 50.0
                                            : 50.0,
                                      ),
                                      children: <Widget>[
                                        Text(
                                          faq.configModel.faq[index].answer,
                                          style: robotoRegular.copyWith(
                                            fontSize:
                                                Responsive.isMobile(context)
                                                    ? Dimensions
                                                        .FONT_SIZE_LARGE
                                                    : Dimensions
                                                        .FONT_SIZE_LARGE_IPAD,
                                          ),
                                          textAlign: TextAlign.justify,
                                        ),
                                      ],
                                    )),
                                  ],
                                ),
                              ],
                            );
                          },
                        );
                      }),
                )
              : NoInternetOrDataScreen(isNoInternet: false)
        ],
      ),
    );
  }
}
