import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/localization_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/wishlist_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/app_constants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/unity.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/inbox_screen.dart';

import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/profile_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/guest_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/all_category_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/web_view_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/app_info_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/html_view_Screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/widget/sign_out_confirmation_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/notification_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/offer/offers_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/address_list_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/profile_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/setting/settings_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/support/support_ticket_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/wishlist/wishlist_screen.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'faq_screen.dart';

class MoreScreen extends StatefulWidget {
  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  bool isGuestMode;
  @override
  void initState() {
    isGuestMode =
        !Provider.of<AuthProvider>(context, listen: false).isLoggedIn();
    if (!isGuestMode) {
      Provider.of<ProfileProvider>(context, listen: false).getUserInfo(context);
      Provider.of<WishListProvider>(context, listen: false).initWishList(
        context,
        Provider.of<LocalizationProvider>(context, listen: false)
            .locale
            .countryCode,
      );
    }

    super.initState();
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      body: Stack(children: [
        // Background
        Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              height: Responsive.isMobile(context) ? 150 : 250,
              color: ColorResources.getPrimary(context),
            )
            // Image.asset(
            //   Images.more_page_header,
            //   height: 150,
            //   fit: BoxFit.fill,
            //   color: Provider.of<ThemeProvider>(context).darkTheme
            //       ? Colors.black
            //       : null,
            // ),
            ),

        // AppBar
        Positioned(
          top: 50,
          // bottom: 0,
          left: Dimensions.PADDING_SIZE_SMALL,
          right: Dimensions.PADDING_SIZE_SMALL,
          child: Consumer<ProfileProvider>(
            builder: (context, profile, child) {
              return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                InkWell(
                  onTap: () {
                    if (isGuestMode) {
                      showAnimatedDialog(context, GuestDialog(), isFlip: true);
                    } else {
                      if (Provider.of<ProfileProvider>(context, listen: false)
                              .userInfoModel !=
                          null) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ProfileScreen()));
                      }
                    }
                  },
                  child: Row(children: [
                    Text(
                      !isGuestMode
                          ? profile.userInfoModel != null
                              ? '${profile.userInfoModel.fName} ${profile.userInfoModel.lName}'
                              : 'Full Name'
                          : 'Guest',
                      style: titilliumRegular.copyWith(
                        color: Provider.of<ThemeProvider>(context).darkTheme
                            ? Colors.black
                            : ColorResources.WHITE,
                        fontSize: Responsive.isMobile(context)
                            ? Dimensions.FONT_SIZE_LARGE
                            : Dimensions.FONT_SIZE_LARGE_IPAD,
                      ),
                    ),
                    SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                    isGuestMode
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image.asset(
                              Images.logo_image,
                              width: Responsive.isMobile(context) ? 35 : 120,
                              height: Responsive.isMobile(context) ? 35 : 120,
                              fit: BoxFit.fill,
                            ),
                          )
                        : profile.userInfoModel == null ||
                                profile.userInfoModel.image == null
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Image.asset(
                                  Images.logo_image,
                                  width:
                                      Responsive.isMobile(context) ? 35 : 120,
                                  height:
                                      Responsive.isMobile(context) ? 35 : 120,
                                  fit: BoxFit.fill,
                                ),
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: CachedNetworkImage(
                                  width:
                                      Responsive.isMobile(context) ? 35 : 120,
                                  height:
                                      Responsive.isMobile(context) ? 35 : 120,
                                  fit: BoxFit.fill,
                                  imageUrl:
                                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.customerImageUrl}/${profile.userInfoModel.image}',
                                  placeholder: (c, o) => CircleAvatar(
                                    child: Image.asset(
                                      Images.logo_image,
                                      width: Responsive.isMobile(context)
                                          ? 35
                                          : 120,
                                      height: Responsive.isMobile(context)
                                          ? 35
                                          : 120,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                  ]),
                ),
              ]);
            },
          ),
        ),

        Container(
          margin:
              EdgeInsets.only(top: Responsive.isMobile(context) ? 120 : 240),
          decoration: BoxDecoration(
            color: ColorResources.getIconBg(context),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          ),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

                  // Top Row Items
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SquareButton(
                          image: Images.shopping_image,
                          title: getTranslated('orders', context),
                          navigateTo: OrderScreen(),
                          count: 1,
                          hasCount: false,
                        ),
                        SquareButton(
                          image: Images.cart_image,
                          title: getTranslated('CART', context),
                          navigateTo: CartScreen(),
                          count:
                              Provider.of<CartProvider>(context, listen: false)
                                  .cartList
                                  .length,
                          hasCount: true,
                        ),
                        SquareButton(
                          image: Images.offers,
                          title: getTranslated('offers', context),
                          navigateTo: OffersScreen(),
                          count: 0,
                          hasCount: false,
                        ),
                        SquareButton(
                          image: Images.wishlist,
                          title: getTranslated('wishlist', context),
                          navigateTo: WishListScreen(),
                          count:
                              Provider.of<AuthProvider>(context, listen: false)
                                          .isLoggedIn() &&
                                      Provider.of<WishListProvider>(context,
                                                  listen: false)
                                              .wishList !=
                                          null &&
                                      Provider.of<WishListProvider>(context,
                                                  listen: false)
                                              .wishList
                                              .length >
                                          0
                                  ? Provider.of<WishListProvider>(context,
                                          listen: false)
                                      .wishList
                                      .length
                                  : 0,
                          hasCount: false,
                        ),
                      ]),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),

                  // Buttons
                  TitleButton(
                      image: Images.fast_delivery,
                      title: getTranslated('address', context),
                      navigateTo: AddressListScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.more_image,
                      title: getTranslated('all_category', context),
                      navigateTo: AllCategoryScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.notification_filled,
                      title: getTranslated('notification', context),
                      navigateTo: NotificationScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  //TODO: seller
                  TitleButton(
                      image: Images.chats,
                      title: getTranslated('chats', context),
                      navigateTo: InboxScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.settings,
                      title: getTranslated('settings', context),
                      navigateTo: SettingsScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.preference,
                      title: getTranslated('support_ticket', context),
                      navigateTo: SupportTicketScreen()),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.term_condition,
                      title: getTranslated('terms_condition', context),
                      navigateTo: HtmlViewScreen(
                        title: getTranslated('terms_condition', context),
                        url: Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .termsConditions,
                      )),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.privacy_policy,
                      title: getTranslated('privacy_policy', context),
                      navigateTo: HtmlViewScreen(
                        title: getTranslated('privacy_policy', context),
                        url: Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .termsConditions,
                      )),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.help_center,
                      title: getTranslated('faq', context),
                      navigateTo: FaqScreen(
                        title: getTranslated('faq', context),
                        // url: Provider.of<SplashProvider>(context, listen: false).configModel.staticUrls.faq,
                      )),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),

                  TitleButton(
                      image: Images.about_us,
                      title: getTranslated('about_us', context),
                      navigateTo: HtmlViewScreen(
                        title: getTranslated('about_us', context),
                        url: Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .aboutUs,
                      )),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.contact_us,
                      title: getTranslated('contact_us', context),
                      navigateTo: WebViewScreen(
                        title: getTranslated('contact_us', context),
                        url: Provider.of<SplashProvider>(context, listen: false)
                            .configModel
                            .staticUrls
                            .contactUs,
                      )),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  TitleButton(
                      image: Images.seller,
                      title: getTranslated('become_seller', context),
                      onTap: () {
                        _launchUrl(AppConstants.REGISTER_VEDOR);
                      }),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: Image.asset(
                        Images.logo_image,
                        width: Responsive.isMobile(context) ? 25 : 55,
                        height: Responsive.isMobile(context) ? 25 : 55,
                        fit: BoxFit.fill,
                      ),
                    ),
                    title: Text(getTranslated('app_info', context),
                        style: titilliumRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_LARGE
                              : Dimensions.FONT_SIZE_LARGE_IPAD,
                        )),
                    onTap: () => showAnimatedDialog(context, AppInfoDialog(),
                        isFlip: true),
                  ),
                  SizedBox(
                      height: Responsive.isMobile(context)
                          ? Dimensions.PADDING_SIZE_EXTRA_SMALL
                          : Dimensions.PADDING_SIZE_EXTRA_LARGE_IPAD),
                  isGuestMode
                      ? SizedBox()
                      : ListTile(
                          leading: Image.asset(
                            Images.signout,
                            width: Responsive.isMobile(context) ? 25 : 55,
                            height: Responsive.isMobile(context) ? 25 : 55,
                            fit: BoxFit.fill,
                          ),
                          title: Text(getTranslated('sign_out', context),
                              style: titilliumRegular.copyWith(
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_LARGE
                                    : Dimensions.FONT_SIZE_LARGE_IPAD,
                              )),
                          onTap: () => showAnimatedDialog(
                              context, SignOutConfirmationDialog(),
                              isFlip: true),
                        ),
                ]),
          ),
        ),
      ]),
    );
  }
}

class SquareButton extends StatelessWidget {
  final String image;
  final String title;
  final Widget navigateTo;
  final int count;
  final bool hasCount;

  SquareButton(
      {@required this.image,
      @required this.title,
      @required this.navigateTo,
      @required this.count,
      @required this.hasCount});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 100;
    return InkWell(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (_) => navigateTo)),
      child: Column(children: [
        Container(
          width: width / 4,
          height: width / 4,
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: ColorResources.getPrimary(context),
          ),
          child: Stack(
            alignment: Alignment.center,
            clipBehavior: Clip.none,
            children: [
              Image.asset(
                image,
                color: Theme.of(context).highlightColor,
                height: Responsive.isMobile(context) ? null : 100,
                width: Responsive.isMobile(context) ? null : 100,
              ),
              hasCount
                  ? Positioned(
                      top: -4,
                      right: -4,
                      child: Consumer<CartProvider>(
                          builder: (context, cart, child) {
                        return CircleAvatar(
                          radius: Responsive.isMobile(context) ? 12 : 22,
                          backgroundColor: ColorResources.RED,
                          child: Text(count.toString(),
                              style: titilliumSemiBold.copyWith(
                                color: ColorResources.WHITE,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_LARGE
                                    : Dimensions.FONT_SIZE_LARGE_IPAD,
                              )),
                        );
                      }),
                    )
                  : SizedBox(),
            ],
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Text(title,
              style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_LARGE
                    : Dimensions.FONT_SIZE_LARGE_IPAD,
              )),
        ),
      ]),
    );
  }
}

class TitleButton extends StatelessWidget {
  final String image;
  final String title;
  final Widget navigateTo;
  final Function onTap;
  TitleButton(
      {@required this.image,
      @required this.title,
      @required this.navigateTo,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Image.asset(
          image,
          width: Responsive.isMobile(context) ? 35 : 55,
          height: Responsive.isMobile(context) ? 35 : 55,
          fit: BoxFit.fill,
        ),
        title: Text(title,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_LARGE
                  : Dimensions.FONT_SIZE_LARGE_IPAD,
            )),
        onTap: () {
          onTap != null
              ? onTap()
              : Navigator.push(
                  context,
                  /*PageRouteBuilder(
            transitionDuration: Duration(seconds: 1),
            pageBuilder: (context, animation, secondaryAnimation) => navigateTo,
            transitionsBuilder: (context, animation, secondaryAnimation, child) {
              animation = CurvedAnimation(parent: animation, curve: Curves.bounceInOut);
              return ScaleTransition(scale: animation, child: child, alignment: Alignment.center);
            },
          ),*/
                  MaterialPageRoute(builder: (_) => navigateTo),
                );
        }
        /*onTap: () => Navigator.push(context, PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => navigateTo,
        transitionsBuilder: (context, animation, secondaryAnimation, child) => FadeTransition(opacity: animation, child: child),
        transitionDuration: Duration(milliseconds: 500),
      )),*/
        );
  }
}
