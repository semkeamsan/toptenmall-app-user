import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/search_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/no_internet_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/product_shimmer.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/search_widget.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_drawer.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_product_widget.dart';

import 'package:provider/provider.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<SearchProvider>(context, listen: false).cleanSearchProduct();
    Provider.of<SearchProvider>(context, listen: false).initHistoryList();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      resizeToAvoidBottomInset: true,
      endDrawer: SearchDrawer(),
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 50.0),
        child: Column(
          children: [
            Row(
              children: [
                BackButton(
                  color: Theme.of(context).textTheme.bodyText1.color,
                  onPressed: () => Navigator.of(context).pop(),
                ),
                Expanded(
                  child: SearchWidget(
                    hintText: getTranslated('SEARCH_NOW', context),
                    onSubmit: (String text) {
                      Provider.of<SearchProvider>(context, listen: false)
                          .searchProduct(text, context);
                      Provider.of<SearchProvider>(context, listen: false)
                          .saveSearchAddress(text);
                    },
                    onClearPressed: () {
                      Provider.of<SearchProvider>(context, listen: false)
                          .cleanSearchProduct();
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.PADDING_SIZE_DEFAULT),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => CartScreen()));
                        },
                        child: Icon(
                          Icons.shopping_cart_outlined,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Consumer<SearchProvider>(
              builder: (context, searchProvider, child) {
                return !searchProvider.isClear
                    ? searchProvider.searchProductList != null
                        ? searchProvider.searchProductList.length > 0
                            ? Expanded(
                                child: SearchProductWidget(
                                    products: searchProvider.searchProductList,
                                    isViewScrollable: true),
                              )
                            : Expanded(
                                child:
                                    NoInternetOrDataScreen(isNoInternet: false),
                              )
                        : Expanded(
                            child: ProductShimmer(
                                isHomePage: false,
                                isEnabled: Provider.of<SearchProvider>(context)
                                        .searchProductList ==
                                    null),
                          )
                    : searchProvider.historyList.length > 0
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Row(
                                //   mainAxisAlignment:
                                //       MainAxisAlignment.spaceBetween,
                                //   children: [
                                //     SizedBox(
                                //       width: 10,
                                //     ),
                                //     InkWell(
                                //       onTap: () {
                                //         //_key.currentState.openEndDrawer();
                                //         showModalBottomSheet(
                                //           context: context,
                                //           isScrollControlled: true,
                                //           backgroundColor: Colors.transparent,
                                //           builder: (c) =>
                                //               SearchFilterBottomSheet(),
                                //         );
                                //       },
                                //       child: Transform.rotate(
                                //         angle: 90 * math.pi / 180,
                                //         child: Icon(Icons.tune),
                                //       ),
                                //     ),
                                //   ],
                                // ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        getTranslated(
                                            'SEARCH_HISTORY', context),
                                        style: robotoBold),
                                    InkWell(
                                        borderRadius: BorderRadius.circular(10),
                                        onTap: () {
                                          Provider.of<SearchProvider>(context,
                                                  listen: false)
                                              .clearSearchAddress();
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(5),
                                            child: Text(
                                              getTranslated('REMOVE', context),
                                              style: titilliumRegular.copyWith(
                                                  fontSize: Dimensions
                                                      .FONT_SIZE_SMALL,
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                            )))
                                  ],
                                ),
                                Consumer<SearchProvider>(
                                  builder: (context, searchProvider, child) =>
                                      Wrap(
                                    children: List.generate(
                                      searchProvider.historyList.length,
                                      ((index) {
                                        return InkWell(
                                          onTap: () {
                                            Provider.of<SearchProvider>(context,
                                                    listen: false)
                                                .searchProduct(
                                                    searchProvider
                                                        .historyList[index],
                                                    context);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 3.0, horizontal: 3.0),
                                            padding: EdgeInsets.symmetric(
                                                vertical: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL,
                                                horizontal: Dimensions
                                                    .PADDING_SIZE_SMALL),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                color: ColorResources.getGrey(
                                                    context)),
                                            child: Text(
                                              Provider.of<SearchProvider>(
                                                          context,
                                                          listen: false)
                                                      .historyList[index] ??
                                                  "",
                                              style: titilliumItalic.copyWith(
                                                  fontSize: Responsive.isMobile(
                                                          context)
                                                      ? Dimensions
                                                          .FONT_SIZE_DEFAULT
                                                      : Dimensions
                                                          .FONT_SIZE_DEFAULT_IPAD),
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : SizedBox(
                            height: 0,
                          );
              },
            ),
          ],
        ),
      ),
    );
  }
}
