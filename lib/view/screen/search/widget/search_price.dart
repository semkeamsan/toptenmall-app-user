import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class SearchPrice extends StatelessWidget {
  const SearchPrice({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        _Item(name: "Min"),
        _Item(name: "Max"),
      ],
    );
  }
}

class _Item extends StatelessWidget {
  final String name;
  final Function onTap;
  const _Item({key, @required this.name, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 120,
        margin: EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
        padding: EdgeInsets.symmetric(
            vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey[300],
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: Dimensions.FONT_SIZE_SMALL),
        ),
      ),
    );
  }
}
