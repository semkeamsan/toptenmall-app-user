import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/search/service_model.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class SearchService extends StatefulWidget {
  const SearchService({key}) : super(key: key);

  @override
  State<SearchService> createState() => _SearchServiceState();
}

class _SearchServiceState extends State<SearchService> {
  int activeIndex;
  @override
  Widget build(BuildContext context) {
    List<ServiceModel> data = [
      ServiceModel(id: "Free Shipping", name: "Free Shipping"),
      ServiceModel(id: "Cash on Delivery", name: "Cash on Delivery"),
    ];
    return Wrap(
      children: List.generate(
        data.length,
        ((index) {
          bool isActive = false;
          if (activeIndex == index) {
            isActive = true;
          }
          return _Item(
            name: data[index].name,
            isActive: isActive,
            onTap: () {
              activeIndex = index;
              setState(() {});
            },
          );
        }),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final String name;
  final bool isActive;
  final Function onTap;
  const _Item({key, @required this.name, this.onTap, this.isActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
        padding: EdgeInsets.symmetric(
            vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          color: this.isActive
              ? ColorResources.getPrimary(context)
              : Colors.grey[300],
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: Dimensions.FONT_SIZE_SMALL,
              color: this.isActive ? Colors.white : Colors.black),
        ),
      ),
    );
  }
}
