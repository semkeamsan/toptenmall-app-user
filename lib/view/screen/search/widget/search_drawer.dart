import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_color.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_filter_bottom_sheet.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_filter_right_sheet.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_price.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_rating.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_service.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/search/widget/search_size.dart';

class SearchDrawer extends StatelessWidget {
  const SearchDrawer({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          // Container(
          //   child: Column(
          //     children: [
          //       SizedBox(
          //         height: 100.0,
          //       ),
          //       Container(
          //         child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: Row(
          //                   children: [Text("Size")],
          //                 ),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: SearchSize(),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          
          //             ]),
          //       ),
          //       SizedBox(
          //         height: 15.0,
          //       ),
          //       Container(
          //         child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: Row(
          //                   children: [Text("Color Family")],
          //                 ),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: SearchColor(),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          
          //             ]),
          //       ),
          //       SizedBox(
          //         height: 15.0,
          //       ),
          //       Container(
          //         child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: Row(
          //                   children: [Text("Service")],
          //                 ),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: SearchService(),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          
          //             ]),
          //       ),
          //       SizedBox(
          //         height: 15.0,
          //       ),
          //       Container(
          //         child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: Row(
          //                   children: [Text("Price")],
          //                 ),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: SearchPrice(),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          
          //             ]),
          //       ),
          //       SizedBox(
          //         height: 15.0,
          //       ),
          //       Container(
          //         child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             children: [
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: Row(
          //                   children: [Text("Rating")],
          //                 ),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //               Padding(
          //                 padding: EdgeInsets.symmetric(
          //                     horizontal: Dimensions.PADDING_SIZE_DEFAULT),
          //                 child: SearchRating(),
          //               ),
          //               SizedBox(
          //                 height: 15.0,
          //               ),
          //             ]),
          //       ),
          //     ],
          //   ),
          // ),
          // SearchFilterBottomSheet(),
          SizedBox(
            height: 100.0,
          ),
          SearchFilterRightSheet()
          // Expanded(
          //   child: Padding(
          //     padding: const EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
          //     child: Align(
          //       alignment: Alignment.bottomCenter,
          //       child: Row(children: [
          //         RaisedButton(
          //           onPressed: () {},
          //           textColor: Colors.white,
          //           padding: const EdgeInsets.all(0.0),
          //           shape: RoundedRectangleBorder(
          //               borderRadius: BorderRadius.circular(1.0)),
          //           child: Container(
          //             width: 120,
          //             decoration: const BoxDecoration(
          //                 gradient: LinearGradient(
          //                   colors: <Color>[
          //                     Color.fromARGB(255, 226, 163, 27),
          //                     Color.fromARGB(255, 226, 163, 27),
          //                     Color.fromARGB(255, 231, 228, 222),
          //                   ],
          //                 ),
          //                 borderRadius: BorderRadius.all(Radius.circular(1.0))),
          //             padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          //             child: const Text(
          //               'Reset',
          //               style:
          //                   TextStyle(fontSize: Dimensions.FONT_SIZE_DEFAULT),
          //               textAlign: TextAlign.center,
          //             ),
          //           ),
          //         ),
          //         RaisedButton(
          //           onPressed: () {},
          //           textColor: Colors.white,
          //           padding: const EdgeInsets.all(0.0),
          //           shape: RoundedRectangleBorder(
          //               borderRadius: BorderRadius.circular(1.0)),
          //           child: Container(
          //             width: 120,
          //             decoration: const BoxDecoration(
          //                 gradient: LinearGradient(
          //                   colors: <Color>[
          //                     Color.fromARGB(255, 204, 212, 236),
          //                     ColorResources.PRIMARY_MATERIAL,
          //                     ColorResources.PRIMARY_MATERIAL,
          //                   ],
          //                 ),
          //                 borderRadius: BorderRadius.all(Radius.circular(1.0))),
          //             padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          //             child: const Text(
          //               'Done',
          //               style:
          //                   TextStyle(fontSize: Dimensions.FONT_SIZE_DEFAULT),
          //               textAlign: TextAlign.center,
          //             ),
          //           ),
          //         ),
          //       ]),
          //     ),
          //   ),
          // ),
          // SizedBox(
          //   height: 50.0,
          // )
        ],
      ),
    );
  }
}
