import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/search/size_model.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class SearchSize extends StatefulWidget {
  const SearchSize({key}) : super(key: key);

  @override
  State<SearchSize> createState() => _SearchSizeState();
}

class _SearchSizeState extends State<SearchSize> {
  int activeIndex;
  @override
  Widget build(BuildContext context) {
    List<SizeModel> data = [
      SizeModel(id: "S", name: "S"),
      SizeModel(id: "M", name: "M"),
      SizeModel(id: "L", name: "L"),
      SizeModel(id: "XL", name: "XL"),
    ];
    return Wrap(
      children: List.generate(
        data.length,
        ((index) {
          bool isActive = false;
          if (activeIndex == index) {
            isActive = true;
          }
          return _Item(
            name: data[index].name,
            isActive: isActive,
            onTap: () {
              activeIndex = index;
              setState(() {});
            },
          );
        }),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final String name;
  final bool isActive;
  final Function onTap;
  const _Item({key, @required this.name, this.onTap, this.isActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
        padding: EdgeInsets.symmetric(
            vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          color: this.isActive
              ? ColorResources.getPrimary(context)
              : Colors.grey[300],
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: Dimensions.FONT_SIZE_SMALL,
              color: this.isActive ? Colors.white : Colors.black),
        ),
      ),
    );
  }
}
