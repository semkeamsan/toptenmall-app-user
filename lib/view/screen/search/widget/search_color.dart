import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/search/color_model.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class SearchColor extends StatefulWidget {
  SearchColor({key}) : super(key: key);

  @override
  State<SearchColor> createState() => _SearchColorState();
}

class _SearchColorState extends State<SearchColor> {
  int activeIndex;
  @override
  Widget build(BuildContext context) {
    List<ColorModel> data = [
      ColorModel(id: "Black", name: "Black"),
      ColorModel(id: "Red", name: "Red"),
      ColorModel(id: "White", name: "White"),
      ColorModel(id: "Yellow", name: "Yellow"),
      ColorModel(id: "Blue", name: "Blue"),
      ColorModel(id: "Green", name: "Green"),
    ];

    return Wrap(
      children: List.generate(
        data.length,
        ((index) {
          bool isActive = false;
          if (activeIndex == index) {
            isActive = true;
          }
          return _Item(
            name: data[index].name,
            isActive: isActive,
            onTap: () {
              activeIndex = index;
              setState(() {});
            },
          );
        }),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final String name;
  final bool isActive;
  final Function onTap;

  const _Item({key, @required this.name, this.onTap, this.isActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
        padding: EdgeInsets.symmetric(
            vertical: Dimensions.PADDING_SIZE_EXTRA_SMALL,
            horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          color: this.isActive
              ? ColorResources.getPrimary(context)
              : Colors.grey[300],
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: Dimensions.FONT_SIZE_SMALL,
              color: this.isActive ? Colors.white : Colors.black),
        ),
      ),
    );
  }
}
