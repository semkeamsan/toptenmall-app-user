import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class SearchRating extends StatelessWidget {
  const SearchRating({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        _Item(),
        _Item(),
        _Item(),
        _Item(),
        _Item(),
        _Item(isUp: true),
      ],
    );
  }
}

class _Item extends StatelessWidget {
  final bool isUp;
  final Function onTap;
  const _Item({key, this.onTap, this.isUp = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 3.0),
        child: isUp
            ? Text("& Up")
            : Icon(
                Icons.star,
                color: Colors.grey[200],
              ),
      ),
    );
  }
}
