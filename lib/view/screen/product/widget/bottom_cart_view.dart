import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/cart_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/show_custom_snakbar.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/widget/cart_bottom_sheet.dart';
import 'package:provider/provider.dart';

class BottomCartView extends StatelessWidget {
  final Product product;
  BottomCartView({@required this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 2.0),
      decoration: BoxDecoration(
        color: Theme.of(context).highlightColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey[
                  Provider.of<ThemeProvider>(context).darkTheme ? 700 : 300],
              blurRadius: 15,
              spreadRadius: 1)
        ],
      ),
      child: Row(children: [
        // Expanded(flex: 6, child: InkWell(
        //   onTap: () {
        //     showModalBottomSheet(context: context, isScrollControlled: true, backgroundColor: Colors.transparent, builder: (con) => CartBottomSheet(product: product, isBuy: true));
        //   },
        //   child: Container(
        //     height: 50,
        //     margin: EdgeInsets.only(left: 5),
        //     alignment: Alignment.center,
        //     decoration: BoxDecoration(
        //       border: Border.all(width: 2, color: ColorResources.getPrimary(context)),
        //       borderRadius: BorderRadius.circular(10),
        //       color: Theme.of(context).highlightColor,
        //     ),
        //     child: Text(
        //       getTranslated('buy_now', context),
        //       style: titilliumSemiBold.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE, color: ColorResources.getPrimary(context)),
        //     ),
        //   ),
        // )),
        Expanded(
          flex: 11,
          child: InkWell(
            onTap: () {
              /*if(!Provider.of<CartProvider>(context, listen: false).isAddedInCart(product.id)) {
              showModalBottomSheet(context: context, isScrollControlled: true, backgroundColor: Colors.transparent, builder: (con) => CartBottomSheet(product: product, isBuy: false, callback: (){
                showCustomSnackBar('Added to cart', context, isError: false);
              }));
            }else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(getTranslated('already_added', context)), backgroundColor: Colors.red));
            }*/
              showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                builder: (con) => CartBottomSheet(
                  product: product,
                  callback: () {
                    showCustomSnackBar(
                        getTranslated('added_to_cart', context), context,
                        isError: false);
                  },
                ),
              );
            },
            child: Container(
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 20.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: ColorResources.getPrimary(context)),
              child: Text(
                getTranslated('add_to_cart', context),
                style: titilliumSemiBold.copyWith(
                    fontSize: Dimensions.FONT_SIZE_LARGE,
                    color: Theme.of(context).highlightColor),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
