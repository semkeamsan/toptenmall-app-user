import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/network_info.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/all_category_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/inbox_screen.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/home_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/more/more_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/notification/notification_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/order/order_screen.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  PageController _pageController = PageController();
  int _pageIndex = 0;

  List<Widget> _screens;

  GlobalKey<ScaffoldMessengerState> _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    _screens = [
      HomePage(),
      AllCategoryScreen(isBackButtonExist: false),
      InboxScreen(isBackButtonExist: false),
      OrderScreen(isBacButtonExist: false),
      //NotificationScreen(isBacButtonExist: false),
      MoreScreen(),
    ];

    NetworkInfo.checkConnectivity(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_pageIndex != 0) {
          _setPage(0);
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: ColorResources.getYellow(context),
          unselectedItemColor: Colors.white.withOpacity(.8),
          selectedFontSize: Responsive.isMobile(context)
              ? Dimensions.FONT_SIZE_DEFAULT
              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
          unselectedFontSize: Responsive.isMobile(context)
              ? Dimensions.FONT_SIZE_DEFAULT
              : Dimensions.FONT_SIZE_DEFAULT_IPAD,
          showUnselectedLabels: true,
          currentIndex: _pageIndex,
          type: BottomNavigationBarType.fixed,
          items: [
            _barItem(Images.home_image, getTranslated('home', context), 0),
            _barItem(Images.more_image, getTranslated('CATEGORY', context), 1),
            _barItem(
                Images.message_image, getTranslated('MESSAGE', context), 2),
            _barItem(
                Images.shopping_image, getTranslated('orders', context), 3),
            //_barItem(Images.notification, getTranslated('notification', context), 3),
            _barItem(Images.edit, getTranslated('more', context), 4),
          ],
          onTap: (int index) {
            _setPage(index);
          },
        ),
        body: PageView.builder(
          controller: _pageController,
          itemCount: _screens.length,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return _screens[index];
          },
        ),
      ),
    );
  }

  BottomNavigationBarItem _barItem(String icon, String label, int index) {
    return BottomNavigationBarItem(
      icon: Image.asset(
        icon,
        color: index == _pageIndex
            ? ColorResources.getYellow(context)
            : Colors.white.withOpacity(.5),
        height: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
        width: Responsive.isMobile(context)
            ? Dimensions.ICON_SIZE_DEFAULT
            : Dimensions.ICON_SIZE_DEFAULT_IPAD,
      ),
      label: label,
    );
  }

  void _setPage(int pageIndex) {
    setState(() {
      _pageController.jumpToPage(pageIndex);
      _pageIndex = pageIndex;
    });
  }
}
