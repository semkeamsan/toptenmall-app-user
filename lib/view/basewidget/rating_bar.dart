import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class RatingBar extends StatelessWidget {
  final double rating;

  RatingBar({@required this.rating});

  @override
  Widget build(BuildContext context) {
    List<Widget> _starList = [];

    int realNumber = rating.floor();
    int partNumber = ((rating - realNumber) * 10).ceil();

    for (int i = 0; i < 5; i++) {
      if (i < realNumber) {
        _starList.add(Icon(Icons.star, color: Colors.orange, size: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,));
      } else if (i == realNumber) {
        _starList.add(SizedBox(
          height:  Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,
          width: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Icon(Icons.star, color: Colors.orange, size: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,),
              ClipRect(
                clipper: _Clipper(part: partNumber),
                child: Icon(Icons.star, color: Colors.grey, size: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,),
              )
            ],
          ),
        ));
      } else {
        _starList.add(Icon(Icons.star, color: Colors.grey, size: Responsive.isMobile(context)
                          ? Dimensions.ICON_SIZE_DEFAULT
                          : Dimensions.ICON_SIZE_DEFAULT_IPAD,));
      }
    }

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: _starList,
    );
  }
}

class _Clipper extends CustomClipper<Rect> {
  final int part;

  _Clipper({@required this.part});

  @override
  Rect getClip(Size size) {
    return Rect.fromLTRB(
      (size.width / 10) * part,
      0.0,
      size.width,
      size.height,
    );
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) => true;
}
