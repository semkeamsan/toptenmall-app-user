import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/data/model/response/product_model.dart';
import 'package:flutter_sixvalley_ecommerce/helper/price_converter.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/splash_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/product/product_details_screen.dart';
import 'package:provider/provider.dart';

class ProductWidget extends StatelessWidget {
  final Product productModel;
  ProductWidget({@required this.productModel});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 1000),
              pageBuilder: (context, anim1, anim2) =>
                  ProductDetails(product: productModel),
            ));
      },
      child: Container(
        margin: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
          borderRadius:
              BorderRadius.circular(Dimensions.PADDING_SIZE_EXTRA_LARGE),
          color: Theme.of(context).highlightColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 5,
            )
          ],
        ),
        child: Stack(children: [
          Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            // Product Image
            Container(
              // padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
              decoration: BoxDecoration(
                color: ColorResources.getIconBg(context),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT),
                    topRight: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT)),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT),
                  topRight: Radius.circular(Dimensions.PADDING_SIZE_DEFAULT),
                ),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl:
                      '${Provider.of<SplashProvider>(context, listen: false).baseUrls.productThumbnailUrl}/${productModel.thumbnail}',
                  placeholder: (c, o) => Image.asset(
                    Images.placeholder,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),

            // Product Details
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: Dimensions.PADDING_SIZE_SMALL,
                vertical: Dimensions.PADDING_SIZE_DEFAULT,
              ),
              child: Container(
                //height: App.height(context) * 12,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(productModel.name ?? '',
                        style: robotoRegular.copyWith(
                          fontSize: Responsive.isMobile(context)
                              ? Dimensions.FONT_SIZE_SMALL
                              : Dimensions.FONT_SIZE_SMALL_IPAD,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            PriceConverter.convertPrice(
                                context, productModel.unitPrice ?? 0.0,
                                discountType: productModel.discountType,
                                discount: productModel.discount),
                            style: robotoBold.copyWith(
                              color: ColorResources.getRed(context),
                              fontSize: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_SMALL
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                          ),
                        ),
                        Expanded(child: SizedBox.shrink()),
                        // Container(
                        //   child: Row(
                        //     children: [
                        //       Icon(
                        //         Icons.local_shipping_outlined,
                        //         color: ColorResources.getPrimary(context),
                        //       ),
                        //       SizedBox(
                        //         width: 5.0,
                        //       ),
                        //       Column(
                        //         mainAxisAlignment: MainAxisAlignment.center,
                        //         crossAxisAlignment: CrossAxisAlignment.start,
                        //         children: [
                        //           Text(
                        //             "Free".toUpperCase(),
                        //             style: TextStyle(
                        //                 fontWeight: FontWeight.bold,
                        //                 color:
                        //                     ColorResources.getPrimary(context)),
                        //           ),
                        //           Text(
                        //             "Shipping".toUpperCase(),
                        //             style: TextStyle(
                        //                 height: .5,
                        //                 fontSize: 7.0,
                        //                 color:
                        //                     ColorResources.getPrimary(context)),
                        //           ),
                        //         ],
                        //       )
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
                    SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    productModel.discount > 0 && productModel.discount != null
                        ? Container(
                            width: 50,
                            child: Text(
                              PriceConverter.convertPrice(
                                  context, productModel.unitPrice),
                              maxLines: 1,
                              style: robotoBold.copyWith(
                                color: Theme.of(context).hintColor,
                                decoration: TextDecoration.lineThrough,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                              ),
                            ),
                          )
                        : SizedBox.shrink(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.star,
                              color:
                                  Provider.of<ThemeProvider>(context).darkTheme
                                      ? Colors.white
                                      : Colors.orange,
                              size: Responsive.isMobile(context)
                                  ? Dimensions.FONT_SIZE_SMALL
                                  : Dimensions.FONT_SIZE_SMALL_IPAD,
                            ),
                            Text(
                              productModel.rating != null
                                  ? productModel.rating.length != 0
                                      ? double.parse(
                                              productModel.rating[0].average)
                                          .toStringAsFixed(1)
                                      : '0.0'
                                  : '0.0',
                              style: robotoRegular.copyWith(
                                color: Provider.of<ThemeProvider>(context)
                                        .darkTheme
                                    ? Colors.white
                                    : Colors.orange,
                                fontSize: Responsive.isMobile(context)
                                    ? Dimensions.FONT_SIZE_SMALL
                                    : Dimensions.FONT_SIZE_SMALL_IPAD,
                              ),
                            ),
                          ],
                        ),
                        // Icon(
                        //   Icons.more_horiz,
                        //   color: Theme.of(context).hintColor,
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ]),

          // Off

          productModel.discount >= 1
              ? Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    height: 20,
                    padding: EdgeInsets.symmetric(
                        horizontal: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                    decoration: BoxDecoration(
                      color: ColorResources.getPrimary(context),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        PriceConverter.percentageCalculation(
                            context,
                            productModel.unitPrice,
                            productModel.discount,
                            productModel.discountType),
                        style: robotoRegular.copyWith(
                            color: Theme.of(context).highlightColor,
                            fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ]),
      ),
    );
  }
}
