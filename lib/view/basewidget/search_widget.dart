import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/provider/search_provider.dart';
import 'package:flutter_sixvalley_ecommerce/provider/theme_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:provider/provider.dart';

class SearchWidget extends StatelessWidget {
  final String hintText;
  final Function onTextChanged;
  final Function onClearPressed;
  final Function onSubmit;
  SearchWidget(
      {@required this.hintText,
      this.onTextChanged,
      @required this.onClearPressed,
      this.onSubmit});

  @override
  Widget build(BuildContext context) {
    final TextEditingController _controller = TextEditingController(
        text: Provider.of<SearchProvider>(context).searchText);
    return Container(
      padding: EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_EXTRA_SMALL),
      height: 50,
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: Dimensions.PADDING_SIZE_SMALL),
        decoration: BoxDecoration(
          color: ColorResources.getTextBg(context),
          border: Border.all(color: ColorResources.getYellow(context)),
          borderRadius:
              BorderRadius.circular(Dimensions.PADDING_SIZE_EXTRA_LARGE),
        ),
        child: TextFormField(
          controller: _controller,
          onFieldSubmitted: (query) {
            if (onSubmit != null) {
              onSubmit(query);
            }
          },
          onChanged: (query) {
            if (onTextChanged != null) {
              onTextChanged(query);
            }
          },
          textInputAction: TextInputAction.search,
          maxLines: 1,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: hintText,
            isDense: true,
            hintStyle: robotoRegular.copyWith(
              color: Theme.of(context).hintColor,
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_DEFAULT
                  : Dimensions.FONT_SIZE_DEFAULT_IPAD,
            ),
            border: InputBorder.none,
            prefixIcon:
                Provider.of<SearchProvider>(context).searchText.isNotEmpty
                    ? IconButton(
                        icon: Icon(Icons.clear,
                            color: ColorResources.getChatIcon(context)),
                        onPressed: () {
                          onClearPressed();
                          _controller.clear();
                        },
                      )
                    : _controller.text.isNotEmpty
                        ? IconButton(
                            icon: Icon(Icons.clear,
                                color: ColorResources.getChatIcon(context)),
                            onPressed: () {
                              onClearPressed();
                              _controller.clear();
                            },
                          )
                        : SizedBox(),
            suffixIcon: Icon(
              Icons.search,
              color: ColorResources.getYellow(context),
              size: Dimensions.ICON_SIZE_DEFAULT,
            ),
          ),
        ),
      ),
    );
  }
}
