import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/localization/language_constrants.dart';
import 'package:flutter_sixvalley_ecommerce/provider/auth_provider.dart';
import 'package:flutter_sixvalley_ecommerce/utill/color_resources.dart';
import 'package:flutter_sixvalley_ecommerce/utill/images.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/animated_custom_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/basewidget/guest_dialog.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/cart/cart_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/category/all_category_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/chat_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/chat/inbox_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/home/home_screen.dart';
import 'package:flutter_sixvalley_ecommerce/view/screen/profile/profile_screen.dart';
import 'package:provider/provider.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  final int activeIndex;
  BottomNavigationBarWidget({this.activeIndex = 0});
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: ColorResources.getPrimary(context),
      currentIndex: activeIndex,
      selectedItemColor: ColorResources.getYellow(context),
      unselectedItemColor: Colors.white,
      items: [
        BottomNavigationBarItem(
          icon: Image.asset(
            Images.home_image,
            color: activeIndex == 0
                ? ColorResources.getYellow(context)
                : Colors.white,
            height: 20.0,
          ),
          label: getTranslated('home', context),
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            Images.more_image,
            color: activeIndex == 1
                ? ColorResources.getYellow(context)
                : Colors.white,
            height: 20.0,
          ),
          label: getTranslated('CATEGORY', context),
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            Images.message_image,
            color: activeIndex == 2
                ? ColorResources.getYellow(context)
                : Colors.white,
            height: 20.0,
          ),
          label: getTranslated('MESSAGE', context),
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            Images.cart_image,
            color: activeIndex == 3
                ? ColorResources.getYellow(context)
                : Colors.white,
            height: 25.0,
          ),
          label: getTranslated('CART', context),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.manage_accounts,
            size: 26.0,
            color: activeIndex == 4
                ? ColorResources.getYellow(context)
                : Colors.white,
          ),
          label: getTranslated('ACCOUNT', context),
        ),
      ],
      onTap: (int index) {
        print("======= Bottom Nav === index: $index");
        switch (index) {
          case 0:
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => HomePage(),
              ),
            );
            break;
          case 1:
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => AllCategoryScreen(),
              ),
            );
            break;

          case 2:
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => InboxScreen(
                  isBackButtonExist: true,
                ),
              ),
            );
            break;

          case 3:
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => CartScreen(),
              ),
            );
            break;

          case 4:
            if (Provider.of<AuthProvider>(context, listen: false)
                .isLoggedIn()) {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => ProfileScreen(),
                ),
              );
            } else {
              showAnimatedDialog(context, GuestDialog(), isFlip: true);
            }

            break;
        }
      },
    );
  }
}
