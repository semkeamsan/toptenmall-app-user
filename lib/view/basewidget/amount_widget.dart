import 'package:flutter/material.dart';
import 'package:flutter_sixvalley_ecommerce/helper/responsive.dart';
import 'package:flutter_sixvalley_ecommerce/utill/custom_themes.dart';
import 'package:flutter_sixvalley_ecommerce/utill/dimensions.dart';

class AmountWidget extends StatelessWidget {
  final String title;
  final String amount;
  final bool isDiscount;

  AmountWidget({@required this.title, @required this.amount, this.isDiscount = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: Dimensions.PADDING_SIZE_EXTRA_SMALL),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(title,
            style: titilliumRegular.copyWith(
              fontSize: Responsive.isMobile(context)
                  ? Dimensions.FONT_SIZE_LARGE
                  : Dimensions.FONT_SIZE_LARGE_IPAD,
            )),
        Text((isDiscount? '-' :'')+ amount,
            style: titilliumRegular.copyWith(
                fontSize: Responsive.isMobile(context)
                    ? Dimensions.FONT_SIZE_LARGE
                    : Dimensions.FONT_SIZE_LARGE_IPAD)),
      ]),
    );
  }
}
